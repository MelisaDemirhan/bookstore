﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
/**
*  @author  : Merve Katı
*  @number  : 152120171025
*  @mail    : mervekati350@gmail.com
*  @date    : 28.05.2021
*  @brief   : this class represent start screen.
*/
namespace BookStore
{
    public partial class StartForm : Form
    {
        ActiveUser active = ActiveUser.getInstance();
        public StartForm()
        {
            InitializeComponent();
            paneluser.Controls.Add(ListAdvice());
        }
        public void adminOpen()
        {
            buttonAdmin.Visible = true;
        }
       
        public static Button staticbutton, staticbutton1,staticbutton2,staticbutton3;
        public static Button staticbutton4,exitbutton,staticbutton5,staticbutton6;
        public static Panel paneltemp;
        private void StartForm_Load(object sender, EventArgs e)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.IsMdiContainer = true;
            staticbutton = buttonLogin;
            staticbutton1 = buttonSignup;
            staticbutton2 = buttonCategories;
            staticbutton3 = buttonCart;

            exitbutton = bttnexitStart;
            staticbutton5 = buttonAdmin;
            staticbutton6 = buttonOrders;
            paneltemp = panelmotion;
            buttonCategories.Visible = false;
            buttonCart.Visible = false;
            buttonAdmin.Visible = false;
            buttonOrders.Visible = false;
            panelmotion.Height = buttonhome.Height;
            panelmotion.Top = buttonhome.Top;

            Home hm = new Home();
            hm.Dock = DockStyle.Fill;
            paneluser.Controls.Add(hm);
            paneluser.Controls["Home"].BringToFront();
            bttnexitStart.BringToFront();
            
        }
        /// <summary>
        /// This function for redirect to the user according to options..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonhome_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Home");
            panelmotion.Height = buttonhome.Height;
            panelmotion.Top = buttonhome.Top;
            Home hm = new Home();
            hm.Dock = DockStyle.Fill;
            paneluser.Controls.Add(hm);
            paneluser.Controls["Home"].BringToFront();
            bttnexitStart.BringToFront();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Login");
            panelmotion.Height = buttonLogin.Height;
            panelmotion.Top = buttonLogin.Top;
            if (ActiveUser.getInstance().Customer == null)
            {
                buttonLogin.Text = "Login";
                UserAuthentication us = new UserAuthentication();
                us.TopLevel = false;
                paneluser.Controls.Add(us);
                us.Show();
                us.Dock = DockStyle.Fill;
                us.BringToFront();
            }
            else if (ActiveUser.getInstance().Customer != null)
            {

                Account c = new Account();
                paneluser.Controls.Add(c);
                c.Dock = DockStyle.Fill;
                c.BringToFront();
                buttonLogin.Text = "Account";
                buttonSignup.Visible = false;
                bttnexitStart.BringToFront();
                
            }
            bttnexitStart.BringToFront();


        }
        public static void buttonadvice(object sender, EventArgs e)
        {
            ButtonLog.Log("Advice");
            StartForm anaform = (StartForm)Application.OpenForms["StartForm"];
            Button b = (Button)sender;
            StartForm.paneltemp.Height = b.Height;
            StartForm.paneltemp.Top = b.Top;
            Advice ucM = new Advice();
            ucM.Dock = DockStyle.Fill;
            anaform.paneluser.Controls.Add(ucM);
            anaform.paneluser.Controls["Advice"].BringToFront();         
            anaform.bttnexitStart.BringToFront();
        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Orders");
            panelmotion.Height = buttonOrders.Height;
            panelmotion.Top = buttonOrders.Top;
            Orders o = new Orders();         
            o.Dock = DockStyle.Fill;
            paneluser.Controls.Add(o);
            paneluser.Controls["Orders"].BringToFront();
            bttnexitStart.BringToFront();
        }

        private void buttonSignup_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("SignUp");
            panelmotion.Height = buttonSignup.Height;
            panelmotion.Top = buttonSignup.Top;
            UserRegisteration use = new UserRegisteration();
            use.TopLevel = false;
            paneluser.Controls.Add(use);
            use.Show();
            use.Dock = DockStyle.Fill;
            use.BringToFront();
            bttnexitStart.BringToFront();
        }

        private void paneluser_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonCategories_Click(object sender, EventArgs e)
        {           
            ButtonLog.Log("Categories");
            panelmotion.Height = buttonCategories.Height;
            panelmotion.Top = buttonCategories.Top;
            Categories hm = new Categories();
            hm.Dock = DockStyle.Fill;
            paneluser.Controls.Add(hm);
            paneluser.Controls["Categories"].BringToFront();
            bttnexitStart.BringToFront();
        }

        public void buttonCart_Click(object sender, EventArgs e)
        {          
            ButtonLog.Log("Cart");
            panelmotion.Height = buttonCart.Height;
            panelmotion.Top = buttonCart.Top;
            if (paneluser.Controls["ShoppingCrts"] == null)
            {
                paneluser.Controls.Add(ShoppingCrts.UploadShopListToCart());
            }
            else
            {
                paneluser.Controls.RemoveByKey("ShoppingCrts");
                paneluser.Controls.Add(ShoppingCrts.UploadShopListToCart());
            }
            paneluser.Controls["ShoppingCrts"].BringToFront();
            bttnexitStart.BringToFront();         
        }

        public static Advice ListAdvice()
        {
            List<List<Product>> products = new List<List<Product>>();
            products.Add(Categories.UploadBooksInfos());
            products.Add(Categories.UploadMusicsInfos());
            products.Add(Categories.UploadMagazinesInfos());

            Random r = new Random();
            Advice advice = new Advice();
            ChooseProductType ProductType = new ChooseProductType();

            for (int i = 0; i < products.Count; i++)
            {
                int random = r.Next(products[i].Count);
                advice.flowLayoutPanel1.Controls.Add(ProductType.CreateProduct
                    (products[i][random]));
            }
            advice.Dock = DockStyle.Fill;
            return advice;
        }

        private void bttnexitStart_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Exit");
            Application.Exit();
        }

        private void buttonAdmin_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Admin");
            panelmotion.Height = buttonAdmin.Height;
            panelmotion.Top = buttonAdmin.Top;
            AdminControl hm = new AdminControl();
            hm.Dock = DockStyle.Fill;
            paneluser.Controls.Add(hm);
            paneluser.Controls["AdminControl"].BringToFront();
            bttnexitStart.BringToFront();
        }
    }
}
