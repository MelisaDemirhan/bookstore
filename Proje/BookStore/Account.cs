﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// a class for print user's account infos.
/// </summary>
namespace BookStore
{
    public partial class Account : UserControl
    {
        public Account()
        {
            InitializeComponent();
        }

        /// <summary>
        /// To print active user's info to screen.
        /// 
        /// </summary>
        private void Account_Load(object sender, EventArgs e)
        {
            labelId.Text = ActiveUser.getInstance().Customer.UserID;
            labelUserName.Text = ActiveUser.getInstance().Customer.UserName;
            labelemail.Text = ActiveUser.getInstance().Customer.UserEmail;
            labeladress.Text = ActiveUser.getInstance().Customer.UserAddress;
            labelName.Text = ActiveUser.getInstance().Customer.Name;
        }
    }
}


