﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 *@author : Özge Katırcı
 *@number : 152120181021
 *@mail   : ozgekatirci0@gmail.com
 *@date   : 5-29-2021
 *@brief  : this class for payment screen. 
 */

namespace BookStore
{
    public partial class CartConfirm : UserControl
    {
        public CartConfirm()
        {
            InitializeComponent();
        }
        /// <summary> Function to select paymenttype
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxPaymentType.SelectedItem == "Credit Card") 
            {
                groupBox1.Visible = true;
            }
            else
            {
                groupBox1.Visible = false;
            }
        }
        /// <summary> Function to make operations when buy button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void buttonBuy_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Buy");
            Invoice invc = new Invoice();
            Invoice.SendInvoicePdf(textBoxPhoneNumber.Text, textBoxAdress.Text, 
                comboBoxPaymentType.SelectedItem.ToString(), comboBoxInstallment.SelectedItem.ToString());
            ShoppingCrt.cart.sendInvoidcebyEmail();
            ShoppingCrt.cart.sendInvoicebySMS();
            System.Diagnostics.Process.Start("Invoice.pdf");
          
            NumberFormatInfo p = new NumberFormatInfo();
            p.NumberDecimalSeparator = ".";
            SqlQueries db = new SqlQueries();
            db.SaveOrder(ShoppingCrt.cart.PaymentAmount.ToString());
            for (int i = 0; i < ShoppingCart.ItemsToPurchase.Count; i++)
            {
                double totalPrice = Convert.ToDouble((ShoppingCart.ItemsToPurchase[i].Product.price), p) * ShoppingCart.ItemsToPurchase[i].Quantity;
                string productName = ShoppingCart.ItemsToPurchase[i].Product.name;
                int quantity = ShoppingCart.ItemsToPurchase[i].Quantity;
                string unitPrice = ShoppingCart.ItemsToPurchase[i].Product.price.ToString();
                string unitId = ShoppingCart.ItemsToPurchase[i].Product.id.ToString();
                db.SaveOrderDetail(productName, unitPrice, quantity, unitId);
            }
            ShoppingCrt.cart.cancelOrder();
        }
    }
}
