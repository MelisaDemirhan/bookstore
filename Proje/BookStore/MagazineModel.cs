﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * @author  : Valerian Kasibante
 * @number  : 152120181099
 * @mail    : kasiabantevalerian25@gmail.com
 * @date    : 29/05/2021
 * @brief   : this class represents the magazine properties
 */

namespace BookStore
{
    public enum MagazineType
    {
        Actual,
        News,
        Sports,
        Computer,
        Technology,
        Fashion,
        ArtandCulture,
        Design,
        Science,
    }
    public class MagazineModel : Product
    {

        public string issue { get; set; }
        public MagazineType type { get; set; }

        /// <summary> this function constructor.
        /// </summary>
        public MagazineModel(string name, string id, int price, string picture, string issue, string magazineType)
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.picture = picture;
            this.issue = issue;
            this.type = magazineType == MagazineType.Actual.ToString() ? MagazineType.Actual :
                magazineType == MagazineType.Computer.ToString() ? MagazineType.Computer :
                magazineType == MagazineType.Fashion.ToString() ? MagazineType.Fashion :
                magazineType == MagazineType.News.ToString() ? MagazineType.News :
                magazineType == MagazineType.Sports.ToString() ? MagazineType.Sports :
                magazineType == MagazineType.Technology.ToString() ? MagazineType.Technology :
                magazineType == MagazineType.Fashion.ToString() ? MagazineType.Fashion :
                magazineType == MagazineType.ArtandCulture.ToString() ? MagazineType.ArtandCulture :
                magazineType == MagazineType.Design.ToString() ? MagazineType.Design :
                magazineType == MagazineType.Science.ToString() ? MagazineType.Science : MagazineType.Science;

        }
        /// <summary> this function constructor.
        /// </summary>
        public MagazineModel(string _name, int _price)
        {
            base.name = _name;
            base.price = _price;
        }
        /// <summary> this function constructor.
        /// </summary>
        public MagazineModel(string _name, int _price, string _ID, string _picture)
        {
            base.name = _name;
            base.price = _price;
            base.id = _ID;
            base.picture = _picture;
        }
        /// <summary> this function print infos.
        /// </summary>
        public override string printProperties()
        {
            return String.Format($"{name} - {id} - {price} - {type}");
        }
    }







}
