﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
/**
 *@author : Muhammed Suwaneh
 *@number : 152120181098
 *@mail   : suwanehmuhammed1@gmail.com 
 *@date   : 6-4-2021
 *@brief  : starts and closes the connections
 */
namespace BookStore
{
    class SqlConnections
    {
        private SqlConnection connect;
        /// <summary>
        /// This function is getter and setter.
        /// </summary>
        public SqlConnection Connect { get => connect; set => connect = value; }
        /// <summary>
        ///  Database connection function.
        /// </summary>
        public void Connection()
        {
            var connectionString = @"Data Source = SQL5097.site4now.net; Initial Catalog = db_a75060_onlinebookstore; User Id = db_a75060_onlinebookstore_admin; Password = ogu@454545";
            connect = new SqlConnection(connectionString);
        }
        /// <summary>
        ///  Database open connection function.
        /// </summary>
        public void Open()
        {
            connect.Open();
        }
        /// <summary>
        ///  Database close connection function.  
        /// </summary>
        public void Close()
        {
            connect.Close();
        }
    }
}
