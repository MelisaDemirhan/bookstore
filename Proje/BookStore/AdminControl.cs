﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for directed to selected option.
 */
namespace BookStore
{
    public partial class AdminControl : UserControl
    {
        public AdminControl()
        {
            InitializeComponent();
        }
        /// <summary> Function to open  admin user form when button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_AdminUser_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("User");
            panel2.Controls.Clear();
            AdminUser hm = new AdminUser();
            hm.Dock = DockStyle.Fill;
            panel2.Controls.Add(hm);
            panel2.Controls["AdminUser"].BringToFront();        
        }
        /// <summary> Function to open  admin book form when button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBook_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Book");
            panel2.Controls.Clear();
            AdminBook hm = new AdminBook();
            hm.Dock = DockStyle.Fill;
            panel2.Controls.Add(hm);
            panel2.Controls["AdminBook"].BringToFront();
        }
        /// <summary> Function to open  magazine  form when button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMagazine_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Magazine");
            panel2.Controls.Clear();
            AdminMagazine hm = new AdminMagazine();
            hm.Dock = DockStyle.Fill;
            panel2.Controls.Add(hm);
            panel2.Controls["AdminMagazine"].BringToFront();
        }
        /// <summary> Function to open  mmusic  form when button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCD_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("CD");
            panel2.Controls.Clear();
            AdminMusicCD hm = new AdminMusicCD();
            hm.Dock = DockStyle.Fill;
            panel2.Controls.Add(hm);
            panel2.Controls["AdminMusicCD"].BringToFront();
        }
    }

   
}
