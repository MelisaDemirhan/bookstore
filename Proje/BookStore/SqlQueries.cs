﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
/**
 *@author : Muhammed Suwaneh
 *@number : 152120181098
 *@mail   : suwanehmuhammed1@gmail.com 
 *@date   : 6-4-2021
 *@brief  : queries 
 */

namespace BookStore
{
    class SqlQueries
    {
        private SqlConnections connection = new SqlConnections();
        private DataTable customerTable = new DataTable();
        private DataTable bookTable = new DataTable();
        private DataTable magazineTable = new DataTable();
        private DataTable musicTable = new DataTable();
        private DataTable billTable = new DataTable();
        private DataTable billDetailTable = new DataTable();
        private DataTable cartTable = new DataTable();
        private DataTable cartProductTable = new DataTable();
        private CustomerModel customer;
        private BookModel book;
        private MagazineModel magazine;
        private MusicCDModel music;
        private List<Product> books = new List<Product>();
        private List<Product> magazines = new List<Product>();
        private List<Product> musics = new List<Product>();
        private List<CustomerModel> customers = new List<CustomerModel>();


        /// <summary>
        /// This function is for get CustomerList
        /// </summary>
        public List<CustomerModel> getCustomerList()
        {
            return customers;
        }


        /// <summary>
        /// This function is for get active user
        /// </summary>
        /// /// <param name="username">This parameter is active user's username.</param>
        /// /// <param name="password">This parameter is active user's password.</param>
        public CustomerModel getActiveUser(string username, string password)
        {
            try
            {
                customerTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter($"SELECT * FROM dbo.CustomersTable WHERE " +
                    $"[CustomerName] = '{username}' AND [CustomerPassword] = '{password}';", connection.Connect);
                da.Fill(customerTable);
                connection.Close();
                for (int i = 0; i < customerTable.Rows.Count; i++)
                {
                    customer = new CustomerModel(customerTable.Rows[i]["CustomerName"].ToString(), customerTable.Rows[i]["CustomerEmail"].ToString(),
                        customerTable.Rows[i]["CustomerPassword"].ToString(), customerTable.Rows[i]["CustomerAddress"].ToString(),
                        customerTable.Rows[i]["CustomerID"].ToString(), Convert.ToBoolean(customerTable.Rows[i]["IsAdmin"]), customerTable.Rows[i]["Name"].ToString());
                }
                return customer;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        /// <summary>
        /// This function is for take customer from sql
        /// </summary>
        public void CustomerList()
        {
            try
            {

                customerTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * From dbo.CustomersTable", connection.Connect);
                da.Fill(customerTable);
                connection.Close();
                for (int i = 0; i < customerTable.Rows.Count; i++)
                {
                    customer = new CustomerModel(customerTable.Rows[i]["CustomerName"].ToString(), customerTable.Rows[i]["CustomerEmail"].ToString(),
                        customerTable.Rows[i]["CustomerPassword"].ToString(), customerTable.Rows[i]["CustomerAddress"].ToString(),
                        customerTable.Rows[i]["CustomerID"].ToString(), Convert.ToBoolean(customerTable.Rows[i]["IsAdmin"]), customerTable.Rows[i]["Name"].ToString());
                    customers.Add(customer);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// This function is for add new customer
        /// </summary>
        /// /// <param name="customer">This parameter is new customer's info.</param>
        public void AddCustomer(CustomerModel customer)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("INSERT INTO dbo.CustomersTable VALUES(@CustomerName, @CustomerEmail, @CustomerPassword, @CustomerAddress, @IsAdmin,@Name)", connection.Connect);
                command.Parameters.AddWithValue("@CustomerName", customer.UserName);
                command.Parameters.AddWithValue("@CustomerEmail", customer.UserEmail);
                command.Parameters.AddWithValue("@CustomerPassword", customer.UserPassword);
                command.Parameters.AddWithValue("@CustomerAddress", customer.UserAddress);
                command.Parameters.AddWithValue("@IsAdmin", customer.IsAdmin);
                command.Parameters.AddWithValue("@Name", customer.Name);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Registration successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for get book list
        /// </summary>
        public List<Product> getBookList()
        {
            return books;
        }

        /// <summary>
        /// This function is for take books from sql
        /// </summary>
        public void BookList()
        {
            try
            {
                bookTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("select * from dbo.BooksTable", connection.Connect);
                da.Fill(bookTable);
                connection.Close();
                for (int i = 0; i < bookTable.Rows.Count; i++)
                {


                    book = new BookModel(bookTable.Rows[i]["BookName"].ToString(),
                        bookTable.Rows[i]["BookID"].ToString(), Int32.Parse(bookTable.Rows[i]["BookPrice"].ToString()),
                        bookTable.Rows[i]["BookImage"].ToString(),
                        bookTable.Rows[i]["ISBN"].ToString(), bookTable.Rows[i]["Author"].ToString(),
                        bookTable.Rows[i]["Publisher"].ToString(), Int32.Parse(bookTable.Rows[i]["Pages"].ToString()));
                    books.Add(book);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for adding new book
        /// </summary>
        /// <param name="book">This parameter is new book's infos.</param>
        public void AddBook(BookModel book)
        {
            try
            {

                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into dbo.BooksTable" +
                    " values(@BookName, @BookPrice, @ISBN, @Author, @Publisher, @Pages, @BookImage)", connection.Connect);
                command.Parameters.AddWithValue("@BookID", book.id);
                command.Parameters.AddWithValue("@BookName", book.name);
                command.Parameters.AddWithValue("@BookPrice", book.price);
                command.Parameters.AddWithValue("@ISBN", book.ISBNumber);
                command.Parameters.AddWithValue("@Author", book.Author);
                command.Parameters.AddWithValue("@Publisher", book.Publisher);
                command.Parameters.AddWithValue("@Pages", book.Page);
                command.Parameters.AddWithValue("@BookImage", book.picture);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Registration successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for updating books infos
        /// </summary>
        /// <param name="book">This parameter is updated books info.</param>
        public void UpdateBook(BookModel book)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"UPDATE dbo.BooksTable SET BookName='{book.name}', " +
                $"BookPrice='{book.price}', ISBN='{book.ISBNumber}', Author='{book.Author}', " +
                $"Publisher='{book.Publisher}', Pages='{book.Page}', BookImage='{book.picture}' WHERE BookID='{book.id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for deleting books  
        /// </summary>
        /// <param name="book">This parameter is a book that will be delete.</param>
        public void DeleteBook(BookModel book)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"Delete from dbo.BooksTable WHERE [BookID] = '{book.id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Book deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// This function is for get magazine list
        /// </summary>     
        public List<Product> getMagazineList()
        {
            return magazines;
        }

        /// <summary>
        /// This function is for take magazine infos from sql
        /// </summary>
        public void MagazineList()
        {
            try
            {
                magazineTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * from dbo.MagazinesTable", connection.Connect);
                da.Fill(magazineTable);
                connection.Close();
                for (int i = 0; i < magazineTable.Rows.Count; i++)
                {
                    magazine = new MagazineModel(magazineTable.Rows[i]["MagazineName"].ToString(),
                        magazineTable.Rows[i]["MagazineID"].ToString(), Int32.Parse(magazineTable.Rows[i]["MagazinePrice"].ToString()),
                        magazineTable.Rows[i]["MagazineImage"].ToString(), magazineTable.Rows[i]["MagazineIssue"].ToString(),
                        magazineTable.Rows[i]["MagazineType"].ToString());
                    magazines.Add(magazine);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for adding magazines
        /// </summary>
        /// <param name="magazine">This parameter is magazine that will be add.</param>
        public void AddMagazine(MagazineModel magazine)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into dbo.MagazinesTable" +
                    " values(@MagazineName, @MagazinePrice, @MagazineIssue, @MagazineType, @MagazineImage)", connection.Connect);

                command.Parameters.AddWithValue("@MagazineName", magazine.name);
                command.Parameters.AddWithValue("@MagazinePrice", magazine.price);
                command.Parameters.AddWithValue("@MagazineIssue", magazine.issue);
                command.Parameters.AddWithValue("@MagazineType", magazine.type);
                command.Parameters.AddWithValue("@MagazineImage", magazine.picture);

                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Registration successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for updating magazines  
        /// </summary>
        /// <param name="magazine">This parameter is magazine that will be updated.</param>
        public void UpdateMagazine(MagazineModel magazine)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"UPDATE MagazinesTable SET MagazineName='{magazine.name}', " +
                $"MagazinePrice='{magazine.price}', MagazineIssue='{magazine.issue}', MagazineType='{magazine.type}', " +
                $"MagazineImage='{magazine.picture}' WHERE MagazineID='{magazine.id}'", connection.Connect);

                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for deleting magazine
        /// </summary>
        /// <param name="magazine">This parameter is a magazine that will be delete from database.</param>
        public void DeleteMagazine(MagazineModel magazine)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"Delete from dbo.MagazinesTable WHERE [MagazineID] = '{magazine.id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Magazine deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public List<Product> getMusicList()
        {
            return musics;
        }

        /// <summary>
        /// This function is for pulling the MusicCD list from the database.
        /// </summary>
        public void MusicCDList()
        {
            try
            {
                musicTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * from dbo.MusicCdsTable", connection.Connect);
                da.Fill(musicTable);
                connection.Close();
                for (int i = 0; i < musicTable.Rows.Count; i++)
                {
                    music = new MusicCDModel(musicTable.Rows[i]["CDName"].ToString(), musicTable.Rows[i]["CDID"].ToString(),
                       Int32.Parse(musicTable.Rows[i]["CDPrice"].ToString()), musicTable.Rows[i]["MusicImage"].ToString(),
                        musicTable.Rows[i]["Singer"].ToString(), musicTable.Rows[i]["Genre"].ToString());
                    musics.Add(music);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for adding MusicCD to the database.  
        /// </summary>
        /// <param name="music">This parameter is a music that will be add to database.</param>
        public void AddMusicCD(MusicCDModel music)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into dbo.MusicCdsTable" +
                    " values(@CDName, @CDPrice, @Singer, @Genre, @MusicImage)", connection.Connect);

                command.Parameters.AddWithValue("@CDName", music.name);
                command.Parameters.AddWithValue("@CDPrice", music.price);
                command.Parameters.AddWithValue("@Singer", music.singer);
                command.Parameters.AddWithValue("@Genre", music.type);
                command.Parameters.AddWithValue("@MusicImage", music.picture);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Registration successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for updating musicCDs to the database.   
        /// </summary>
        /// <param name="music">This parameter is a music that will be update to database.</param>
        public void UpdateMusicCD(MusicCDModel music)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"UPDATE dbo.MusicCDsTable SET CDName='{music.name}', " +
                $"CDPrice='{music.price}', Singer='{music.singer}', Genre='{music.type.ToString()}', " +
                $"MusicImage='{music.picture}' WHERE CDID='{music.id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for deleting mucicCD from the database.    
        /// </summary>
        /// <param name="music">This parameter is a music that will be delete from database.</param>
        public void DeleteMusicCD(MusicCDModel music)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand($"DELETE FROM dbo.MusicCDsTable WHERE [CDID] = '{music.id}'", connection.Connect);
                command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("MusicCD deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        /// <summary>
        /// This function is for pulling the order list from the database.     
        /// </summary>
        public void OrderList()
        {
            try
            {
                billTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * from dbo.BillTable WHERE [CustomerID] = '" + ActiveUser.getInstance().Customer.UserID+"'", connection.Connect);
                da.Fill(billTable);
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for pulling order details list from the database.    
        /// </summary>
        /// <param name="id">This parameter is bill's id.</param>
        public void OrderDetailList(int id)
        {
            try
            {
                billDetailTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select ProductName,UnitPrice,Quantity from BillProduct where BillId=" + id, connection.Connect);
                da.Fill(billDetailTable);
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        int billId;
        /// <summary>
        /// This function is for adding orders to the database.     
        /// </summary>
        /// <param name="totalPrice">This parameter is bill's Total Price.</param>
        public void SaveOrder(string totalPrice)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into dbo.BillTable values(@CustomerID,@Date,@TotalPrice)", connection.Connect);
                command.Parameters.AddWithValue("@CustomerID", ActiveUser.getInstance().Customer.UserID.ToString());
                command.Parameters.AddWithValue("@Date", DateTime.Now);
                command.Parameters.AddWithValue("@TotalPrice", totalPrice);
                command.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 * FROM BillTable ORDER BY ID DESC", connection.Connect);
                billId = (int)cmd2.ExecuteScalar();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function is for adding orders details to the database.     
        /// </summary>
        /// <param name="productName">This parameter is Product Name that will be add to Bill.</param>
        /// <param name="unitPrice">This parameter is Unit Price that will be add to Bill.</param>
        /// <param name="quantity">This parameter is Quantity that will be add to bill.</param>
        /// <param name="totalPrice">This parameter is Total Price that will be add to bill.</param>
        public void SaveOrderDetail(string productName, string unitPrice, int quantity, string unitId)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into BillProduct values(@ID,@BillId,@ProductName,@UnitPrice,@Quantity)", connection.Connect);
                command.Parameters.AddWithValue("@ID", unitId);
                command.Parameters.AddWithValue("@BillId", billId);
                command.Parameters.AddWithValue("@ProductName", productName);
                command.Parameters.AddWithValue("@UnitPrice", unitPrice);
                command.Parameters.AddWithValue("@Quantity", quantity);
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public DataTable getOrderSource()
        {
            return billTable;
        }
        /// <summary>
        /// This function returns the order details list.
        /// </summary>
        /// <returns>This function returns Datatable</returns>
        public DataTable getOrderDetailSource()
        {
            return billDetailTable;
        }
        public void SaveLog(string click)
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("Insert into ButtonLogTable values(@LogInfo)", connection.Connect);
                command.Parameters.AddWithValue("@LogInfo", click);
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        public void SaveCartProduct(ItemToPurchase itp, string type)
        {
            try
            {             
                CartProductList();
                int temp = 0;
                for (int i = 0; i < cartProductTable.Rows.Count; i++)
                {
                    if (cartProductTable.Rows[i][0].ToString() == itp.Product.id)
                    {
                        temp = 1;
                        break;
                    }

                }
                if (temp == 1) 
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command = new SqlCommand($"UPDATE dbo.CustomersCartProductsTable SET ProductQuantity = ProductQuantity+1 where" +
                    $" [ProductID]= '" + itp.Product.id + "' AND [CustomerID] = '" + ActiveUser.getInstance().Customer.UserID + "'", connection.Connect);
                    command.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command2 = new SqlCommand("Insert into dbo.CustomersCartProductsTable values(@ProductID," +
                    "@ProductName,@ProductPrice,@ProductImg,@ProductQuantity,@CustomerID,@ProductType)", connection.Connect);
                    command2.Parameters.AddWithValue("@ProductID", itp.Product.id);
                    command2.Parameters.AddWithValue("@ProductName", itp.Product.name);
                    command2.Parameters.AddWithValue("@ProductPrice", itp.Product.price);
                    command2.Parameters.AddWithValue("@ProductImg", itp.Product.picture);
                    command2.Parameters.AddWithValue("@ProductQuantity", itp.Quantity);
                    command2.Parameters.AddWithValue("@CustomerID", ActiveUser.getInstance().Customer.UserID);
                    command2.Parameters.AddWithValue("@ProductType", type);
                    command2.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Add successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

               
            }
        }
        public void CartProductList()
        {
            try
            {
                cartProductTable.Clear();
                connection.Connection();
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter("Select * from dbo.CustomersCartProductsTable where CustomerID='" + ActiveUser.getInstance().Customer.UserID+"'", connection.Connect);
                da.Fill(cartProductTable);
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DeleteCartProductList(ItemToPurchase itp,int a)
        {
            try
            {
                connection.Connection();
                connection.Open();
                CartProductList();
                int temp = 0;
                for (int i = 0; i < cartProductTable.Rows.Count; i++)
                {
                    if (cartProductTable.Rows[i][0].ToString() == itp.Product.id && 
                        Int32.Parse(cartProductTable.Rows[i][4].ToString())>1)
                    {
                        temp = 1;
                    }
                }
                if (temp == 1)
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command = new SqlCommand($"UPDATE dbo.CustomersCartProductsTable SET ProductQuantity = ProductQuantity-1 where" +
                    $" [ProductID]= '" + itp.Product.id + "' AND [CustomerID] = '" + ActiveUser.getInstance().Customer.UserID + "'", connection.Connect);
                    command.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Update successful", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                if (temp == 0 || a == 0) 
                {
                    connection.Connection();
                    connection.Open();
                    SqlCommand command2 = new SqlCommand($"DELETE FROM dbo.CustomersCartProductsTable where ProductID = '" + itp.Product.id + "'", connection.Connect);
                    command2.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Product deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void DeleteAllCartProductList()
        {
            try
            {
                connection.Connection();
                connection.Open();
                SqlCommand command = new SqlCommand("DELETE FROM dbo.CustomersCartProductsTable", connection.Connect);
               command.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Table Deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public DataTable getCartProduct()
        {
            return cartProductTable;
        }

    }
}
