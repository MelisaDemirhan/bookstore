﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Merve Katı
*  @number  : 152120171025
*  @mail    : mervekati350@gmail.com
*  @date    : 28.05.2021
*  @brief   : this class for magazine infos
*/

namespace BookStore
{
    public partial class Magazine : UserControl
    {   
        string ID;
        string pic;
        int count = 0;
        SqlQueries sqlQueries = new SqlQueries();
        /// <summary>
        /// This parameter is constructor.
        /// </summary>
        /// <param name="magazine">This parameter is magazine.</param>
        public Magazine(MagazineModel magazine)
        {
            InitializeComponent();
            lblMagazineName.Text = magazine.name;
            lblIssue.Text = magazine.issue;
            lblPrice.Text = magazine.price.ToString();
            lblType.Text = magazine.type.ToString();
            ID = magazine.id;
            img.Load(magazine.picture);          
            pic = magazine.picture;
        }
        /// <summary>
        /// This function adds the selected product to customer's cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddMagazine_Click(object sender, EventArgs e)
        {           
            ButtonLog.Log("AddMagazine");      
            count++;
            lblProductNumber.Text = (count).ToString();
            ItemToPurchase item = new ItemToPurchase();
            item.Product = new MagazineModel(lblMagazineName.Text, int.Parse(lblPrice.Text), ID, pic);
            item.Quantity = 1;
            ShoppingCrt.cart.addProduct(item);
            sqlQueries.SaveCartProduct(item,"Magazine");
        }
        /// <summary>
        /// This function subtracts the selected product from customer's cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubtractMagazine_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("SubstractMagazine");   
            if (count > 0)
            {
                count--;
                lblProductNumber.Text = (count).ToString();
                ItemToPurchase item = new ItemToPurchase();
                item.Product = new MagazineModel(lblMagazineName.Text, int.Parse(lblPrice.Text));
                item.Quantity = 1;
                ShoppingCrt.cart.removeProduct(item);
                sqlQueries.DeleteCartProductList(item,1);
            }
        }
    }
}

