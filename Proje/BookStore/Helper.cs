﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Security.Cryptography;
using System.IO;

/**
 *@author : Muhammed Suwaneh
 *@number : 152120181098
 *@mail   : suwanehmuhammed1@gmail.com 
 *@date   : 5-28-2021
 *@brief  : has all the functions all classes would use 
 */
namespace BookStore
{
    static class Helper
    {
        /// <summary>
        ///  Sets a timeout 
        /// </summary>
        static public void CloseWindow(Form WindowOne, Form WindowTwo, int time)
        {
            var timer = new System.Windows.Forms.Timer();

            timer.Interval = time;
            timer.Tick += (s, e) =>
            {
                WindowTwo.Hide();
                WindowOne.Closed += (ss, args) => WindowTwo.Close();
                WindowOne.Show();
                timer.Stop();
            };

            timer.Start();
        }

        /// <summary>
        /// error label color and message 
        /// </summary>
        /// <param name="label"></param>
        /// <param name="errorMessage"></param>
        static public void ShowErrors(Label label, string errorMessage)
        {
            var timer = new System.Windows.Forms.Timer();

            timer.Interval = 3000;
            timer.Tick += (s, e) =>
            {
                label.Text = errorMessage;
                label.ForeColor = Color.Red;

                timer.Stop();
            };

            timer.Start();
        }

        static public string EncryptData(string data)
        {
            string temp = "";
            MD5 algorithm = MD5.Create();

            byte[] dataInBytes = Encoding.UTF8.GetBytes(data);
            byte[] hashInBytes = algorithm.ComputeHash(dataInBytes);

            for (int i = 0; i < hashInBytes.Length; i++)
            {
                temp += hashInBytes[i];
            }
            return temp;
        }
    }
}
