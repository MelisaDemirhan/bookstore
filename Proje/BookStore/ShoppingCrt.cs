﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
/**
 *@author : Özge Katırcı
 *@number : 152120181021
 *@mail   : ozgekatirci0@gmail.com
 *@date   : 5-29-2021
 *@brief  : this class for shopping cart operations.
 */

namespace BookStore
{
    public partial class ShoppingCrt : UserControl
    {
        public static ShoppingCart cart = new ShoppingCart(ActiveUser.getInstance().Customer.UserID,
          0, "", ShoppingCart.ItemsToPurchase);
        string ID;
        string picBox;
        Product _product;
        public static bool DeleteControl = false;
        public static bool PaymentAmountControl = false;
        public NumberFormatInfo p = new NumberFormatInfo();
        SqlQueries sqlQueries = new SqlQueries();

        /// <summary>
        /// This function created for print ınfos to  screen.
        /// </summary>
        /// <param name="purchase"> item to purchase.</param>
        public ShoppingCrt(ItemToPurchase purchase)
        {
            InitializeComponent();
            try
            {
                p.NumberDecimalSeparator = ".";
                lblName.Text = purchase.Product.name;
                labelProductQuantity.Text = purchase.Quantity.ToString();
                lblUnitPrice.Text = purchase.Product.price + " $";
                labelTotalPrice.Text = ((purchase.Quantity * double.Parse(purchase.Product.price.ToString(), p)) + " $").ToString();
                picCoverPage.Load(purchase.Product.picture);              
                picBox = purchase.Product.picture;
                _product = purchase.Product;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// This function created for products deleted..
        /// </summary>
   
        private void btnCancelItem_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("CancelItem");
            try
            {
                DialogResult dResults = new DialogResult();
                dResults = MessageBox.Show("Products are deleted, are you sure?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dResults == DialogResult.Yes)
                {
                    DeleteControl = true;
                    for (int i = 0; i < int.Parse(labelProductQuantity.Text); i++)
                    {
                        ItemToPurchase item = new ItemToPurchase();
                        item.Product = _product;
                        item.Quantity = 1;
                        cart.removeProduct(item);
                        ShoppingCrts.UploadShopListToCart();
                        sqlQueries.DeleteCartProductList(item,0);
                    }
                    StartForm f = (StartForm)Application.OpenForms["StartForm"];
                    f.buttonCart.PerformClick();
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);             
            }

        }
    }
}
