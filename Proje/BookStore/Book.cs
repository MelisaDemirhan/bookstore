﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 * @author  : Valerian Kasibante
 * @number  : 152120181099
 * @mail    : kasiabantevalerian25@gmail.com
 * @date    : 29/05/2021
 * @brief   : this class represents the book ınfos.
 */

namespace BookStore
{
    public partial class Book : UserControl
    {
        string ID;
        string pic;
       int count = 0;
        SqlQueries sqlQueries = new SqlQueries();
        /// <summary>
        /// This parameter is constructor.
        /// </summary>
        /// <param name="book">This parameter is book.</param>
        public Book(BookModel book)
        {
            InitializeComponent();
            labelBookName.Text = book.name;
            labelAuthor.Text = book.Author;
            labelPublisher.Text = book.Publisher;
            labelPageNumber.Text = book.Page.ToString();
            labelISBN.Text = book.ISBNumber;
            labelPrice.Text = book.price.ToString();
            ID = book.id.ToString();
            img.Load(book.picture);
            pic = book.picture;
        }
        /// <summary>
        ///  function  to adding the selected product to cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddBook_Click(object sender, EventArgs e)
        {            
            ButtonLog.Log("AddBook");
            count++;
            labelProductNumber.Text = (count).ToString();
            ItemToPurchase item = new ItemToPurchase();
            item.Product = new BookModel(labelBookName.Text, int.Parse(labelPrice.Text), ID, pic);
            item.Quantity = 1;
            ShoppingCrt.cart.addProduct(item);
            sqlQueries.SaveCartProduct(item,"Book");
        }

        /// <summary>
        ///   function  to substracting the selected product to cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubtractBook_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("SubtractBook");           
            if (count > 0)
            {
                count--;
                labelProductNumber.Text = (count).ToString();
                ItemToPurchase item = new ItemToPurchase();
                item.Product = new BookModel(labelBookName.Text, int.Parse(labelPrice.Text), ID, pic);
                item.Quantity = 1;
                ShoppingCrt.cart.removeProduct(item);
                sqlQueries.DeleteCartProductList(item,1);
            }
        }    
    }
}
