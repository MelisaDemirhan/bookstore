﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 *@author : Özge Katırcı
 *@number : 152120181021
 *@mail   : ozgekatirci0@gmail.com
 *@date   : 5-29-2021
 *@brief  : this class created to using factory design pattern for product class. 
 */

namespace BookStore
{
    public class ChooseProductType
    {
        /// <summary>
        /// This function create user control to given product.
        /// </summary>
        /// <param name="product">This parameter is a product.</param>
        public UserControl CreateProduct(Product product)
        {
            if (product is BookModel)
            {
                return new Book((BookModel)product);
            }
            else if (product is MusicCDModel)
            {
                return new MusicCd((MusicCDModel)product);
            }
            else if (product is MagazineModel)
            {
                return new Magazine((MagazineModel)product);
            }
            else
                return null;
        }

    }
}
