﻿
namespace BookStore
{
    partial class ShoppingCrts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShoppingCrts));
            this.flowLayoutPanelCrts = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPaymentAmount = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnConfrimCart = new System.Windows.Forms.Button();
            this.btnDeleteCart = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanelCrts
            // 
            this.flowLayoutPanelCrts.AutoScroll = true;
            this.flowLayoutPanelCrts.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanelCrts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.flowLayoutPanelCrts.Location = new System.Drawing.Point(0, 2);
            this.flowLayoutPanelCrts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanelCrts.Name = "flowLayoutPanelCrts";
            this.flowLayoutPanelCrts.Size = new System.Drawing.Size(962, 562);
            this.flowLayoutPanelCrts.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(32, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 33);
            this.label1.TabIndex = 4;
            this.label1.Text = "Payment Amount :";
            // 
            // labelPaymentAmount
            // 
            this.labelPaymentAmount.AutoSize = true;
            this.labelPaymentAmount.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelPaymentAmount.ForeColor = System.Drawing.Color.Black;
            this.labelPaymentAmount.Location = new System.Drawing.Point(241, 16);
            this.labelPaymentAmount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPaymentAmount.Name = "labelPaymentAmount";
            this.labelPaymentAmount.Size = new System.Drawing.Size(0, 33);
            this.labelPaymentAmount.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.labelPaymentAmount);
            this.panel2.Controls.Add(this.btnConfrimCart);
            this.panel2.Controls.Add(this.btnDeleteCart);
            this.panel2.Location = new System.Drawing.Point(0, 568);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(962, 110);
            this.panel2.TabIndex = 2;
            // 
            // btnConfrimCart
            // 
            this.btnConfrimCart.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnConfrimCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConfrimCart.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnConfrimCart.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnConfrimCart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfrimCart.Location = new System.Drawing.Point(285, 53);
            this.btnConfrimCart.Margin = new System.Windows.Forms.Padding(4);
            this.btnConfrimCart.Name = "btnConfrimCart";
            this.btnConfrimCart.Size = new System.Drawing.Size(194, 42);
            this.btnConfrimCart.TabIndex = 3;
            this.btnConfrimCart.Text = "Confirm Cart";
            this.btnConfrimCart.UseVisualStyleBackColor = false;
            this.btnConfrimCart.Click += new System.EventHandler(this.btnConfrimCart_Click);
            // 
            // btnDeleteCart
            // 
            this.btnDeleteCart.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnDeleteCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDeleteCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteCart.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDeleteCart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDeleteCart.Location = new System.Drawing.Point(20, 53);
            this.btnDeleteCart.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteCart.Name = "btnDeleteCart";
            this.btnDeleteCart.Size = new System.Drawing.Size(194, 42);
            this.btnDeleteCart.TabIndex = 2;
            this.btnDeleteCart.Text = "Delete Cart";
            this.btnDeleteCart.UseVisualStyleBackColor = false;
            this.btnDeleteCart.Click += new System.EventHandler(this.btnDeleteCart_Click);
            // 
            // ShoppingCrts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.flowLayoutPanelCrts);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ShoppingCrts";
            this.Size = new System.Drawing.Size(965, 678);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelCrts;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label labelPaymentAmount;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnConfrimCart;
        private System.Windows.Forms.Button btnDeleteCart;
    }
}
