﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Globalization;
using System.Windows.Forms;

/**
*  @author  : Merve Katı
*  @number  : 152120171025
*  @mail    : mervekati350@gmail.com
*  @date    : 28.05.2021
*  @brief   : this class make pdf to send  bill to customer via mail.
*/

namespace BookStore
{

    /// <summary>
    /// This function to make pdf infos..
    /// </summary>
 
    public class Invoice
    {        
      public static void SendInvoicePdf(string _phoneNumber, string _adress, string _paymentType, string _installment)
        {
            try
            {
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 40f, 40f, 80f, 80f);
                PdfWriter.GetInstance(pdfDoc, new FileStream("Invoice.pdf", FileMode.Create));
                pdfDoc.Open();
                pdfDoc.AddCreator("PolyMath");
                pdfDoc.AddCreationDate();
                pdfDoc.AddHeader("PolyMath", "E-Invoice");
                pdfDoc.AddTitle("PolyMath");
                Paragraph spacer = new Paragraph("")
                {
                    SpacingBefore = 30f,
                    SpacingAfter = 30f
                };
                string[] headers = new string[5];
                headers[0] = "ID";
                headers[1] = "Product Name";
                headers[2] = "Quantity";
                headers[3] = "Price";
                headers[4] = "Total Price";

                var timeTable = new PdfPTable(new[] { .75f })
                {
                    HorizontalAlignment = 2,
                    WidthPercentage = 40,
                    DefaultCell = { MinimumHeight = 12f, }
                };
                timeTable.AddCell("Date");
                timeTable.AddCell(DateTime.Now.ToString());
                pdfDoc.Add(timeTable);
                pdfDoc.Add(spacer);
                Paragraph elements1 = new Paragraph("Bill to :" +
                    Environment.NewLine + "PolyMath" +
                    Environment.NewLine + "Root" +
                    Environment.NewLine + "Eskisehir Osmangazi University " +
                    Environment.NewLine + "Computer Engineering Department " +
                    Environment.NewLine + "4444 4444 4444" +
                    Environment.NewLine + "polymathfatura@outlook.com" +
                    Environment.NewLine);
                Paragraph elements2 = new Paragraph("Ship to :" +
                    Environment.NewLine + "Customer ID: " + ActiveUser.getInstance().Customer.UserID +
                    Environment.NewLine + ActiveUser.getInstance().Customer.Name +
                    Environment.NewLine + _adress +
                    Environment.NewLine + _phoneNumber +
                    Environment.NewLine + _paymentType);



                elements1.Alignment = Element.ALIGN_LEFT;
                elements2.Alignment = Element.ALIGN_RIGHT;
                pdfDoc.Add(elements1);
                pdfDoc.Add(elements2);
                pdfDoc.Add(spacer);
                PdfPTable table = new PdfPTable(new[] { .75f, 2.5f, 1f, 1f, 1.5f })
                {
                    HorizontalAlignment = 1,
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f, }
                };
                for (int i = 0; i < headers.Length; i++)
                {
                    PdfPCell cell = new PdfPCell(new PdfPCell());
                    cell.AddElement(new Chunk(headers[i]));
                    table.AddCell(cell);
                }
                double totalCost = 0;
                for (int i = 0; i < ShoppingCart.ItemsToPurchase.Count; i++)
                {
                    double TotalPrice = Convert.ToDouble((ShoppingCart.ItemsToPurchase[i].Product.price), provider) * ShoppingCart.ItemsToPurchase[i].Quantity;
                    totalCost += TotalPrice;
                    table.AddCell(ShoppingCart.ItemsToPurchase[i].Product.id);
                    table.AddCell(ShoppingCart.ItemsToPurchase[i].Product.name);
                    table.AddCell(ShoppingCart.ItemsToPurchase[i].Quantity.ToString());
                    table.AddCell(ShoppingCart.ItemsToPurchase[i].Product.price.ToString());
                    table.AddCell(TotalPrice.ToString());
                }
                pdfDoc.Add(table);
                Paragraph elements3 = new Paragraph("Adress :" + _adress +
                    Environment.NewLine + "Phone Number :" + _phoneNumber +
                    Environment.NewLine + "Installment :" + _installment +
                    Environment.NewLine + "Total Cost =" + totalCost);
                elements3.Alignment = Element.ALIGN_RIGHT;
                pdfDoc.Add(elements3);
                pdfDoc.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
