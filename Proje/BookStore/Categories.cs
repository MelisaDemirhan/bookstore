﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for redirect to user selected options.
 */
namespace BookStore
{
    public partial class Categories : UserControl
    {
        
        public Categories()
        {
            InitializeComponent();
        }

        /// <summary> Function open magazine form when magazine button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMagazine_Click(object sender, EventArgs e)
        {            
            ButtonLog.Log("Magazine");
            Magazines ucM = new Magazines();
            ucM.Dock = DockStyle.Fill;
            panelCat.Controls.Add(ucM);
            panelCat.Controls["Magazines"].BringToFront();     
        }
        /// <summary> Function open form when book button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBooks_Click(object sender, EventArgs e)
        {           
            ButtonLog.Log("Book");
            Books ucM = new Books();
                ucM.Dock = DockStyle.Fill;
                panelCat.Controls.Add(ucM);
                panelCat.Controls["Books"].BringToFront();    
        }
        /// <summary> Function open form when music cd button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMusicCDs_Click(object sender, EventArgs e)
        {            
            ButtonLog.Log("MusicCD");
            MusicCds ucM = new MusicCds();
                ucM.Dock = DockStyle.Fill;
                panelCat.Controls.Add(ucM);
                panelCat.Controls["MusicCds"].BringToFront();            
        }

        /// <summary> Function to make operations when form load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Categories_Load(object sender, EventArgs e)
        {
            panelCat.Controls.Clear();
            panelCat.AutoScroll = true;
            try
            {               
                panelCat.Controls.Add(UploadAllBooksInfos(UploadBooksInfos()));
                panelCat.Controls.Add(UploadAllMusicsInfos(UploadMusicsInfos()));
                panelCat.Controls.Add(UploadAllMagazinesInfos(UploadMagazinesInfos()));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function to list book ınfos.
        /// </summary>
        /// <returns>This function returns Product List</returns>
        public static List<Product> UploadBooksInfos()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.BookList();
            return sq1.getBookList();
        }
        /// <summary>
        /// This function to list musicCds ınfos.
        /// </summary>
        /// <returns>This function returns Product List</returns>
        public static List<Product> UploadMusicsInfos()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.MusicCDList();
            return sq1.getMusicList();
        }
        /// <summary>
        /// / This function to list magazine ınfos.
        /// </summary>
        /// <returns>This function returns Product List</returns>
        public static List<Product> UploadMagazinesInfos()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.MagazineList();
            return sq1.getMagazineList();
        }
        /// <summary>
        ///  function to list books on the books form.
        /// </summary>
        /// <param name="BookList">This parameter is Product List.</param>
        /// <returns>This function returns User Control Books</returns>
        public static Books UploadAllBooksInfos(List<Product> BookList)
        {
            Books book = new Books();
            foreach (BookModel _book in BookList)
            {
                book.flowLayoutPanelMusicCds.Controls.Add(new Book(_book));
            }
            book.Dock = DockStyle.Fill;
            return book;
        }
        /// <summary>
        ///  function to list magazines on the magazines form. 
        /// </summary>
        /// <param name="MusicCDList">This parameter is Product List.</param>
        /// <returns>This function returns User Control MusicCDs.</returns>
        public static MusicCds UploadAllMusicsInfos(List<Product> MusicCDList)
        {
            MusicCds cd = new MusicCds();
            foreach (MusicCDModel musidCD in MusicCDList)
            {
                cd.flowLayoutPanelMusicCds.Controls.Add(new MusicCd(musidCD));
            }
            cd.Dock = DockStyle.Fill;
            return cd;
        }
        /// <summary>
        ///  function to list magazines on the magazines form.
        /// </summary>
        /// <param name="MagazineList">This parameter is Product List.</param>
        /// <returns>This function returns User Control Magazines.</returns>
        public static Magazines UploadAllMagazinesInfos(List<Product> MagazineList)
        {
            Magazines mg = new Magazines();
            foreach (MagazineModel magazine in MagazineList)
            {
                mg.flowLayoutPanelMagazines.Controls.Add(new Magazine(magazine));
            }
            mg.Dock = DockStyle.Fill;
            return mg;
        }

    }
}
