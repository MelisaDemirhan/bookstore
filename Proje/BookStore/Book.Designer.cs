﻿
namespace BookStore
{
    partial class Book
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Book));
            this.labelProductNumber = new System.Windows.Forms.Label();
            this.btnSubtractBook = new System.Windows.Forms.Button();
            this.btnAddBook = new System.Windows.Forms.Button();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelISBN = new System.Windows.Forms.Label();
            this.labelPageNumber = new System.Windows.Forms.Label();
            this.labelPublisher = new System.Windows.Forms.Label();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.labelBookName = new System.Windows.Forms.Label();
            this.img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProductNumber
            // 
            this.labelProductNumber.AutoSize = true;
            this.labelProductNumber.BackColor = System.Drawing.Color.Transparent;
            this.labelProductNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelProductNumber.ForeColor = System.Drawing.Color.Black;
            this.labelProductNumber.Location = new System.Drawing.Point(97, 404);
            this.labelProductNumber.Name = "lblProductNumber";
            this.labelProductNumber.Size = new System.Drawing.Size(17, 18);
            this.labelProductNumber.TabIndex = 32;
            this.labelProductNumber.Text = "0";
            // 
            // btnSubtractBook
            // 
            this.btnSubtractBook.BackColor = System.Drawing.Color.Transparent;
            this.btnSubtractBook.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubtractBook.BackgroundImage")));
            this.btnSubtractBook.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubtractBook.FlatAppearance.BorderSize = 0;
            this.btnSubtractBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubtractBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSubtractBook.Location = new System.Drawing.Point(59, 401);
            this.btnSubtractBook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSubtractBook.Name = "btnSubtractBook";
            this.btnSubtractBook.Size = new System.Drawing.Size(31, 21);
            this.btnSubtractBook.TabIndex = 31;
            this.btnSubtractBook.Text = "-";
            this.btnSubtractBook.UseVisualStyleBackColor = false;
            this.btnSubtractBook.Click += new System.EventHandler(this.btnSubtractBook_Click);
            // 
            // btnAddBook
            // 
            this.btnAddBook.BackColor = System.Drawing.Color.Transparent;
            this.btnAddBook.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddBook.BackgroundImage")));
            this.btnAddBook.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddBook.FlatAppearance.BorderSize = 0;
            this.btnAddBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddBook.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAddBook.Location = new System.Drawing.Point(127, 401);
            this.btnAddBook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddBook.Name = "btnAddBook";
            this.btnAddBook.Size = new System.Drawing.Size(31, 21);
            this.btnAddBook.TabIndex = 30;
            this.btnAddBook.UseVisualStyleBackColor = false;
            this.btnAddBook.Click += new System.EventHandler(this.btnAddBook_Click);
            // 
            // lblPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelPrice.ForeColor = System.Drawing.Color.Black;
            this.labelPrice.Location = new System.Drawing.Point(40, 357);
            this.labelPrice.Name = "lblPrice";
            this.labelPrice.Size = new System.Drawing.Size(0, 33);
            this.labelPrice.TabIndex = 29;
            // 
            // lblISBN
            // 
            this.labelISBN.AutoSize = true;
            this.labelISBN.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelISBN.ForeColor = System.Drawing.Color.Black;
            this.labelISBN.Location = new System.Drawing.Point(40, 325);
            this.labelISBN.Name = "lblISBN";
            this.labelISBN.Size = new System.Drawing.Size(0, 33);
            this.labelISBN.TabIndex = 28;
            // 
            // lblPageNumber
            // 
            this.labelPageNumber.AutoSize = true;
            this.labelPageNumber.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelPageNumber.ForeColor = System.Drawing.Color.Black;
            this.labelPageNumber.Location = new System.Drawing.Point(40, 289);
            this.labelPageNumber.Name = "lblPageNumber";
            this.labelPageNumber.Size = new System.Drawing.Size(0, 33);
            this.labelPageNumber.TabIndex = 27;
            // 
            // lblPublisher
            // 
            this.labelPublisher.AutoSize = true;
            this.labelPublisher.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelPublisher.ForeColor = System.Drawing.Color.Black;
            this.labelPublisher.Location = new System.Drawing.Point(40, 257);
            this.labelPublisher.Name = "lblPublisher";
            this.labelPublisher.Size = new System.Drawing.Size(0, 33);
            this.labelPublisher.TabIndex = 26;
            // 
            // lblAuthor
            // 
            this.labelAuthor.AutoSize = true;
            this.labelAuthor.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelAuthor.ForeColor = System.Drawing.Color.Black;
            this.labelAuthor.Location = new System.Drawing.Point(40, 226);
            this.labelAuthor.Name = "lblAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(0, 33);
            this.labelAuthor.TabIndex = 25;
            // 
            // lblBookName
            // 
            this.labelBookName.AutoSize = true;
            this.labelBookName.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelBookName.ForeColor = System.Drawing.Color.Black;
            this.labelBookName.Location = new System.Drawing.Point(40, 193);
            this.labelBookName.Name = "lblBookName";
            this.labelBookName.Size = new System.Drawing.Size(0, 33);
            this.labelBookName.TabIndex = 24;
            // 
            // picCoverPage
            // 
            this.img.Location = new System.Drawing.Point(40, 23);
            this.img.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.img.Name = "picCoverPage";
            this.img.Size = new System.Drawing.Size(165, 165);
            this.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img.TabIndex = 17;
            this.img.TabStop = false;
            // 
            // Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.labelProductNumber);
            this.Controls.Add(this.btnSubtractBook);
            this.Controls.Add(this.btnAddBook);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelISBN);
            this.Controls.Add(this.labelPageNumber);
            this.Controls.Add(this.labelPublisher);
            this.Controls.Add(this.labelAuthor);
            this.Controls.Add(this.labelBookName);
            this.Controls.Add(this.img);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Book";
            this.Size = new System.Drawing.Size(277, 430);
            ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelProductNumber;
        private System.Windows.Forms.Button btnSubtractBook;
        private System.Windows.Forms.Button btnAddBook;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelISBN;
        private System.Windows.Forms.Label labelPageNumber;
        private System.Windows.Forms.Label labelPublisher;
        private System.Windows.Forms.Label labelAuthor;
        private System.Windows.Forms.Label labelBookName;
        private System.Windows.Forms.PictureBox img;
    }
}
