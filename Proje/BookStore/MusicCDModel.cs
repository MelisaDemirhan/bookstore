﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * @author  : Valerian Kasibante
 * @number  : 152120181099
 * @mail    : kasiabantevalerian25@gmail.com
 * @date    : 29/05/2021
 * @brief   : this class represents the Music Information
 */

namespace BookStore
{
    public enum MusicType
    {
        Pop,
        Rock,
        Classic,
        HipHop,
        Slow,
        Jazz,
    }
    public class MusicCDModel : Product
    {

        public string singer { get; set; }
        public MusicType type { get; set; }
        /// <summary> this function constructor.
        /// </summary>
        public MusicCDModel(string name, string id, int price, string picture, string singer, string musicType)
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.picture = picture;
            this.singer = singer;
            this.type = musicType == MusicType.Pop.ToString() ? MusicType.Pop :
                musicType == MusicType.Rock.ToString() ? MusicType.Rock :
                musicType == MusicType.Classic.ToString() ? MusicType.Classic :
                musicType == MusicType.HipHop.ToString() ? MusicType.HipHop :
                musicType == MusicType.Slow.ToString() ? MusicType.Slow :
                musicType == MusicType.Jazz.ToString() ? MusicType.Jazz : MusicType.Jazz;
        }

        public MusicCDModel(string _name, int _price)
        {
            base.name = _name;
            base.price = _price;
        }
        /// <summary> this function constructor.
        /// </summary>
        public MusicCDModel(string _name, int _price, string _ID, string _coverPage)
        {
            base.name = _name;
            base.price = _price;
            base.id = _ID;
            base.picture = _coverPage;
        }
        /// <summary> this function print infos.
        /// </summary>
        public override string printProperties()
        {
            return String.Format($"{name} - {price} - {singer} - {type}");
        }
    }

}





