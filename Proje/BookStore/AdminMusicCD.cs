﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for adding  music Cds to database, updating music Cds from databsse, and deleting music Cds from database.
 */
namespace BookStore
{
    public partial class AdminMusicCD : UserControl
    {
        public AdminMusicCD()
        {
            InitializeComponent();
        }

        string namex, idx, singerx, typex;
        int pricex;
        string imgx;
        public static int selectedIndex = -1;
        public static int index = -1;
        SqlQueries sq = new SqlQueries();
        string name, price, genre, singer, image, id;

        /// <summary> Function to list music cds.. 
        /// </summary>
        /// <param name="MusicCds"></param>

        private void ListCD(List<Product> MusicCds)
        {
            try
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < MusicCds.Count; i++)
                {
                    MusicCDModel mcd = new MusicCDModel(namex, idx, pricex, imgx, singerx, typex);
                    mcd = (MusicCDModel)MusicCds[i];
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = mcd.id;
                    dataGridView1.Rows[i].Cells[1].Value = mcd.name;
                    dataGridView1.Rows[i].Cells[2].Value = mcd.price;
                    dataGridView1.Rows[i].Cells[3].Value = mcd.singer;
                    dataGridView1.Rows[i].Cells[4].Value = mcd.type;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to add new music cds to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Add");
            try
            {
                bool exist = false;
                List<Product> BKList = UploadCD();
                if (exist == false)
                {
                    if (textBoxName.Text != "" && textBoxSinger.Text != "" && textBoxPrice.Text != "" && pictureBox1.Image != null && textBoxURL.Text != "")
                    {
                        name = textBoxName.Text;
                        price = textBoxPrice.Text;
                        genre = comboBoxGenre.SelectedItem.ToString();
                        singer = textBoxSinger.Text;
                        image = textBoxURL.Text;
                        MusicCDModel cd = new MusicCDModel(name, "", Int32.Parse(price), image, singer, genre);
                        sq.AddMusicCD(cd);
                        ListCD(BKList);
                    }
                    else
                    {
                        MessageBox.Show("Please! Fill all fields!");
                    }
                }
                ClearTextBox();
                AdminMusicCD_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to update  music cds from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Update");
            try
            {
                selectedIndex = dataGridView1.CurrentCell.RowIndex;
                name = textBoxName.Text;
                price = textBoxPrice.Text;
                genre = comboBoxGenre.SelectedItem.ToString();
                singer = textBoxSinger.Text;
                id = dataGridView1.Rows[selectedIndex].Cells[0].Value.ToString();
                image = textBoxURL.Text;
                MusicCDModel cd = new MusicCDModel(name, id, Int32.Parse(price), image, singer, genre);
                sq.UpdateMusicCD(cd);
                List<Product> BKList = UploadCD();
                if (BKList.Count != 0)
                {
                    BKList.Clear();
                }
                dataGridView1.Show();
                selectedIndex = -1;
                ListCD(BKList);
                AdminMusicCD_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to delete  music cds from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Delete");
            try
            {
                dataGridView1.FirstDisplayedCell = null;
                dataGridView1.ClearSelection();
                List<Product> CDList = UploadCD();
                if (CDList.Count != 0)
                {
                    for (int i = 0; i < CDList.Count; i++)
                    {
                        selectedIndex = dataGridView1.CurrentCell.RowIndex;
                        name = dataGridView1.Rows[selectedIndex].Cells[1].Value.ToString();
                        price = dataGridView1.Rows[selectedIndex].Cells[2].Value.ToString();
                        genre = dataGridView1.Rows[selectedIndex].Cells[4].Value.ToString();
                        singer = dataGridView1.Rows[selectedIndex].Cells[3].Value.ToString();
                        id = dataGridView1.Rows[selectedIndex].Cells[0].Value.ToString();


                        MusicCDModel cd = new MusicCDModel(name, id, Int32.Parse(price), "", singer, genre);
                        sq.DeleteMusicCD(cd);
                        if (CDList.Count != 0)
                        {
                            CDList.Clear();
                        }
                        dataGridView1.Rows.RemoveAt(selectedIndex);
                        selectedIndex = -1;
                        ListCD(CDList);
                        AdminMusicCD_Load(sender, e);
                    }
                }
                else
                {
                    MessageBox.Show("CD is Empty!");
                    buttonDelete.Enabled = false;
                    buttonUpdate.Enabled = false;
                }
                ClearTextBox();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to load picture.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Load");
            try
            {
                pictureBox1.Load(textBoxURL.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to make operations when form load. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void AdminMusicCD_Load(object sender, EventArgs e)
        {
            try
            {
                List<Product> CDList = UploadCD();
                ListCD(CDList);
                dataGridView1.Show();
                dataGridView1.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to clear database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }
        /// <summary> Function to get infos selected index on the datagridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                List<Product> CDList = UploadCD();
                if (CDList.Count != 0)
                {
                    index = e.RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    textBoxName.Text = row.Cells[1].Value.ToString();
                    textBoxPrice.Text = row.Cells[2].Value.ToString();
                    textBoxSinger.Text = row.Cells[3].Value.ToString();
                    comboBoxGenre.SelectedItem = row.Cells[4].Value.ToString();

                    foreach (var item in CDList)
                    {
                        if (item.id == row.Cells[0].Value.ToString())
                        {
                            textBoxURL.Text = item.picture;
                            pictureBox1.Load(item.picture);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("CD is empty!");
                    buttonDelete.Enabled = false;
                    buttonUpdate.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        ///Function to clear textboxes.
        /// </summary>
        private void ClearTextBox()
        {
            textBoxName.Text = "";
            textBoxPrice.Text = "";
            textBoxSinger.Text = "";
            textBoxURL.Text = "";
            comboBoxGenre.SelectedItem = "";
            comboBoxGenre.SelectedIndex = -1;
            pictureBox1.Image = null;
        }
        /// <summary>
        ///Function to get music infos from database.
        /// </summary>
        public static List<Product> UploadCD()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.MusicCDList();
            return sq1.getMusicList();
        }
    }
}
