﻿
namespace BookStore
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartForm));
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonAdmin = new System.Windows.Forms.Button();
            this.panelmotion = new System.Windows.Forms.Panel();
            this.buttonCategories = new System.Windows.Forms.Button();
            this.buttonCart = new System.Windows.Forms.Button();
            this.buttonSignup = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonhome = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.paneluser = new System.Windows.Forms.Panel();
            this.bttnexitStart = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.paneluser.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.buttonOrders);
            this.panel2.Controls.Add(this.buttonAdmin);
            this.panel2.Controls.Add(this.panelmotion);
            this.panel2.Controls.Add(this.buttonCategories);
            this.panel2.Controls.Add(this.buttonCart);
            this.panel2.Controls.Add(this.buttonSignup);
            this.panel2.Controls.Add(this.buttonLogin);
            this.panel2.Controls.Add(this.buttonhome);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(235, 678);
            this.panel2.TabIndex = 1;
            // 
            // buttonOrders
            // 
            this.buttonOrders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonOrders.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonOrders.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonOrders.FlatAppearance.BorderSize = 0;
            this.buttonOrders.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrders.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrders.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonOrders.Location = new System.Drawing.Point(13, 498);
            this.buttonOrders.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Size = new System.Drawing.Size(220, 54);
            this.buttonOrders.TabIndex = 9;
            this.buttonOrders.Text = "Orders";
            this.buttonOrders.UseVisualStyleBackColor = true;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonAdmin
            // 
            this.buttonAdmin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonAdmin.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonAdmin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonAdmin.FlatAppearance.BorderSize = 0;
            this.buttonAdmin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdmin.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdmin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonAdmin.Location = new System.Drawing.Point(13, 560);
            this.buttonAdmin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAdmin.Name = "buttonAdmin";
            this.buttonAdmin.Size = new System.Drawing.Size(220, 54);
            this.buttonAdmin.TabIndex = 7;
            this.buttonAdmin.Text = "Admin";
            this.buttonAdmin.UseVisualStyleBackColor = true;
            this.buttonAdmin.Click += new System.EventHandler(this.buttonAdmin_Click);
            // 
            // panelmotion
            // 
            this.panelmotion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(141)))), ((int)(((byte)(123)))));
            this.panelmotion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelmotion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelmotion.Location = new System.Drawing.Point(0, 199);
            this.panelmotion.Margin = new System.Windows.Forms.Padding(4);
            this.panelmotion.Name = "panelmotion";
            this.panelmotion.Size = new System.Drawing.Size(13, 54);
            this.panelmotion.TabIndex = 2;
            // 
            // buttonCategories
            // 
            this.buttonCategories.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonCategories.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonCategories.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonCategories.FlatAppearance.BorderSize = 0;
            this.buttonCategories.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonCategories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCategories.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCategories.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonCategories.Location = new System.Drawing.Point(13, 384);
            this.buttonCategories.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCategories.Name = "buttonCategories";
            this.buttonCategories.Size = new System.Drawing.Size(220, 54);
            this.buttonCategories.TabIndex = 6;
            this.buttonCategories.Text = "Cotegories";
            this.buttonCategories.UseVisualStyleBackColor = true;
            this.buttonCategories.Click += new System.EventHandler(this.buttonCategories_Click);
            // 
            // buttonCart
            // 
            this.buttonCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonCart.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonCart.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonCart.FlatAppearance.BorderSize = 0;
            this.buttonCart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCart.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonCart.Location = new System.Drawing.Point(13, 446);
            this.buttonCart.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCart.Name = "buttonCart";
            this.buttonCart.Size = new System.Drawing.Size(220, 54);
            this.buttonCart.TabIndex = 5;
            this.buttonCart.Text = "Cart";
            this.buttonCart.UseVisualStyleBackColor = true;
            this.buttonCart.Click += new System.EventHandler(this.buttonCart_Click);
            // 
            // buttonSignup
            // 
            this.buttonSignup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonSignup.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonSignup.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonSignup.FlatAppearance.BorderSize = 0;
            this.buttonSignup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonSignup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSignup.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSignup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonSignup.Location = new System.Drawing.Point(13, 322);
            this.buttonSignup.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSignup.Name = "buttonSignup";
            this.buttonSignup.Size = new System.Drawing.Size(220, 54);
            this.buttonSignup.TabIndex = 3;
            this.buttonSignup.Text = "Sign up";
            this.buttonSignup.UseVisualStyleBackColor = true;
            this.buttonSignup.Click += new System.EventHandler(this.buttonSignup_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonLogin.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonLogin.FlatAppearance.BorderSize = 0;
            this.buttonLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogin.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonLogin.Location = new System.Drawing.Point(13, 261);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(220, 54);
            this.buttonLogin.TabIndex = 2;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonhome
            // 
            this.buttonhome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonhome.Cursor = System.Windows.Forms.Cursors.Default;
            this.buttonhome.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(195)))), ((int)(((byte)(181)))));
            this.buttonhome.FlatAppearance.BorderSize = 0;
            this.buttonhome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonhome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonhome.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonhome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.buttonhome.Location = new System.Drawing.Point(13, 199);
            this.buttonhome.Margin = new System.Windows.Forms.Padding(4);
            this.buttonhome.Name = "buttonhome";
            this.buttonhome.Size = new System.Drawing.Size(220, 54);
            this.buttonhome.TabIndex = 1;
            this.buttonhome.Text = "Home";
            this.buttonhome.UseVisualStyleBackColor = true;
            this.buttonhome.Click += new System.EventHandler(this.buttonhome_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 178);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // paneluser
            // 
            this.paneluser.Controls.Add(this.bttnexitStart);
            this.paneluser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneluser.Location = new System.Drawing.Point(235, 0);
            this.paneluser.Margin = new System.Windows.Forms.Padding(4);
            this.paneluser.Name = "paneluser";
            this.paneluser.Size = new System.Drawing.Size(965, 678);
            this.paneluser.TabIndex = 3;
            this.paneluser.Paint += new System.Windows.Forms.PaintEventHandler(this.paneluser_Paint);
            // 
            // bttnexitStart
            // 
            this.bttnexitStart.BackColor = System.Drawing.Color.Transparent;
            this.bttnexitStart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bttnexitStart.BackgroundImage")));
            this.bttnexitStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bttnexitStart.FlatAppearance.BorderSize = 0;
            this.bttnexitStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bttnexitStart.ForeColor = System.Drawing.Color.Transparent;
            this.bttnexitStart.Location = new System.Drawing.Point(923, 0);
            this.bttnexitStart.Margin = new System.Windows.Forms.Padding(4);
            this.bttnexitStart.Name = "bttnexitStart";
            this.bttnexitStart.Size = new System.Drawing.Size(41, 32);
            this.bttnexitStart.TabIndex = 0;
            this.bttnexitStart.UseVisualStyleBackColor = false;
            this.bttnexitStart.Click += new System.EventHandler(this.bttnexitStart_Click);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1200, 678);
            this.Controls.Add(this.paneluser);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StartForm";
            this.Load += new System.EventHandler(this.StartForm_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.paneluser.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonhome;
        private System.Windows.Forms.Button buttonSignup;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonCategories;
        private System.Windows.Forms.Panel panelmotion;
        public System.Windows.Forms.Button buttonAdmin;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button buttonCart;
        public System.Windows.Forms.Button buttonOrders;
        public System.Windows.Forms.Panel paneluser;
        public System.Windows.Forms.Button bttnexitStart;
    }
}