﻿
namespace BookStore
{
    partial class Magazine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Magazine));
            this.lblProductNumber = new System.Windows.Forms.Label();
            this.btnSubtractMagazine = new System.Windows.Forms.Button();
            this.btnAddMagazine = new System.Windows.Forms.Button();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblIssue = new System.Windows.Forms.Label();
            this.lblMagazineName = new System.Windows.Forms.Label();
            this.img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProductNumber
            // 
            this.lblProductNumber.AutoSize = true;
            this.lblProductNumber.BackColor = System.Drawing.Color.Transparent;
            this.lblProductNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblProductNumber.ForeColor = System.Drawing.Color.Black;
            this.lblProductNumber.Location = new System.Drawing.Point(98, 353);
            this.lblProductNumber.Name = "lblProductNumber";
            this.lblProductNumber.Size = new System.Drawing.Size(17, 18);
            this.lblProductNumber.TabIndex = 44;
            this.lblProductNumber.Text = "0";
            // 
            // btnSubtractMagazine
            // 
            this.btnSubtractMagazine.BackColor = System.Drawing.Color.Transparent;
            this.btnSubtractMagazine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubtractMagazine.BackgroundImage")));
            this.btnSubtractMagazine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubtractMagazine.FlatAppearance.BorderSize = 0;
            this.btnSubtractMagazine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubtractMagazine.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSubtractMagazine.Location = new System.Drawing.Point(58, 353);
            this.btnSubtractMagazine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSubtractMagazine.Name = "btnSubtractMagazine";
            this.btnSubtractMagazine.Size = new System.Drawing.Size(31, 21);
            this.btnSubtractMagazine.TabIndex = 43;
            this.btnSubtractMagazine.UseVisualStyleBackColor = false;
            this.btnSubtractMagazine.Click += new System.EventHandler(this.btnSubtractMagazine_Click);
            // 
            // btnAddMagazine
            // 
            this.btnAddMagazine.BackColor = System.Drawing.Color.Transparent;
            this.btnAddMagazine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddMagazine.BackgroundImage")));
            this.btnAddMagazine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddMagazine.FlatAppearance.BorderSize = 0;
            this.btnAddMagazine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddMagazine.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAddMagazine.Location = new System.Drawing.Point(133, 353);
            this.btnAddMagazine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddMagazine.Name = "btnAddMagazine";
            this.btnAddMagazine.Size = new System.Drawing.Size(31, 21);
            this.btnAddMagazine.TabIndex = 42;
            this.btnAddMagazine.UseVisualStyleBackColor = false;
            this.btnAddMagazine.Click += new System.EventHandler(this.btnAddMagazine_Click);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPrice.ForeColor = System.Drawing.Color.Black;
            this.lblPrice.Location = new System.Drawing.Point(40, 300);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(0, 33);
            this.lblPrice.TabIndex = 41;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblType.ForeColor = System.Drawing.Color.Black;
            this.lblType.Location = new System.Drawing.Point(40, 268);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 33);
            this.lblType.TabIndex = 40;
            // 
            // lblIssue
            // 
            this.lblIssue.AutoSize = true;
            this.lblIssue.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblIssue.ForeColor = System.Drawing.Color.Black;
            this.lblIssue.Location = new System.Drawing.Point(40, 236);
            this.lblIssue.Name = "lblIssue";
            this.lblIssue.Size = new System.Drawing.Size(0, 33);
            this.lblIssue.TabIndex = 39;
            // 
            // lblMagazineName
            // 
            this.lblMagazineName.AutoSize = true;
            this.lblMagazineName.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMagazineName.ForeColor = System.Drawing.Color.Black;
            this.lblMagazineName.Location = new System.Drawing.Point(40, 204);
            this.lblMagazineName.Name = "lblMagazineName";
            this.lblMagazineName.Size = new System.Drawing.Size(0, 33);
            this.lblMagazineName.TabIndex = 38;
            // 
            // picCoverPage
            // 
            this.img.Location = new System.Drawing.Point(46, 12);
            this.img.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.img.Name = "picCoverPage";
            this.img.Size = new System.Drawing.Size(165, 165);
            this.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img.TabIndex = 33;
            this.img.TabStop = false;
            // 
            // Magazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lblProductNumber);
            this.Controls.Add(this.btnSubtractMagazine);
            this.Controls.Add(this.btnAddMagazine);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.lblIssue);
            this.Controls.Add(this.lblMagazineName);
            this.Controls.Add(this.img);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Magazine";
            this.Size = new System.Drawing.Size(277, 430);
            ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblProductNumber;
        private System.Windows.Forms.Button btnSubtractMagazine;
        private System.Windows.Forms.Button btnAddMagazine;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblIssue;
        private System.Windows.Forms.Label lblMagazineName;
        private System.Windows.Forms.PictureBox img;
    }
}
