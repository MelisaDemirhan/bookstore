﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
/**
 *@author : Muhammed Suwaneh
 *@number : 152120181098
 *@mail   : suwanehmuhammed1@gmail.com 
 *@date   : 5-29-2021
 *@brief  : this class to save user's motions. 
 */

namespace BookStore
{
    public class ButtonLog
    {
        /// <summary> Function to save ınfos when user clicked to button.
        /// </summary>
        /// <param name="buttonName"></param>

        public static void Log(string buttonName)
        {
            if (ActiveUser.getInstance().Customer != null)
            {
                string fileName = @"Logs.txt";
                string writeText = ActiveUser.getInstance().Customer.UserName + " click " + buttonName + " button at " + DateTime.Now.ToString();
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
                fs.Close();
                File.AppendAllText(fileName, writeText + Environment.NewLine);
                SqlQueries sqlQueries = new SqlQueries();
                sqlQueries.SaveLog(writeText);
            }
        }
    }
}
