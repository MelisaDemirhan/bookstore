var searchData=
[
  ['book_132',['Book',['../class_book_store_1_1_book.html#a750f376caceeba3f48fbe595447bd35a',1,'BookStore::Book']]],
  ['booklist_133',['BookList',['../class_book_store_1_1_sql_queries.html#a9b56dbdff17cfbdf1add62113a694258',1,'BookStore::SqlQueries']]],
  ['bookmodel_134',['BookModel',['../class_book_store_1_1_book_model.html#a95a1d28626370e804b95afd823f4f4b8',1,'BookStore.BookModel.BookModel(string name, string id, int price, string picture, string isbn, string author, string publisher, int pages)'],['../class_book_store_1_1_book_model.html#abdb92130bd180222f2436fcc9496d0e3',1,'BookStore.BookModel.BookModel(string _name, int _price, string _ID, string _coverPage)']]]
];
