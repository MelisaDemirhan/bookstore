var searchData=
[
  ['getactiveuser_145',['getActiveUser',['../class_book_store_1_1_sql_queries.html#acc7f3dc66ec503b90d35bcb0844b787e',1,'BookStore::SqlQueries']]],
  ['getbooklist_146',['getBookList',['../class_book_store_1_1_sql_queries.html#a21661036c87233008642fde4d9f5667b',1,'BookStore::SqlQueries']]],
  ['getcustomer_147',['GetCustomer',['../class_book_store_1_1_customer_model.html#ae42942c329a40fd0239c662c8af198a3',1,'BookStore::CustomerModel']]],
  ['getcustomerlist_148',['getCustomerList',['../class_book_store_1_1_sql_queries.html#a8b6b7432fc1da3905b6ef8c41566bb85',1,'BookStore::SqlQueries']]],
  ['getinstance_149',['getInstance',['../class_book_store_1_1_active_user.html#a2ce0b6a78779221044365e536c88a37d',1,'BookStore::ActiveUser']]],
  ['getmagazinelist_150',['getMagazineList',['../class_book_store_1_1_sql_queries.html#a75583958e9075d1d257f31df8d3560c8',1,'BookStore::SqlQueries']]],
  ['getorderdetailsource_151',['getOrderDetailSource',['../class_book_store_1_1_sql_queries.html#abafe392bb829d325dd61811e87de0d61',1,'BookStore::SqlQueries']]]
];
