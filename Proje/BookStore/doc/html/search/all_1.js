var searchData=
[
  ['book_13',['Book',['../class_book_store_1_1_book.html#a750f376caceeba3f48fbe595447bd35a',1,'BookStore.Book.Book()'],['../class_book_store_1_1_book.html',1,'BookStore.Book']]],
  ['booklist_14',['BookList',['../class_book_store_1_1_sql_queries.html#a9b56dbdff17cfbdf1add62113a694258',1,'BookStore::SqlQueries']]],
  ['bookmodel_15',['BookModel',['../class_book_store_1_1_book_model.html#a95a1d28626370e804b95afd823f4f4b8',1,'BookStore.BookModel.BookModel(string name, string id, int price, string picture, string isbn, string author, string publisher, int pages)'],['../class_book_store_1_1_book_model.html#abdb92130bd180222f2436fcc9496d0e3',1,'BookStore.BookModel.BookModel(string _name, int _price, string _ID, string _coverPage)'],['../class_book_store_1_1_book_model.html',1,'BookStore.BookModel']]],
  ['books_16',['Books',['../class_book_store_1_1_books.html',1,'BookStore']]],
  ['bookstore_17',['BookStore',['../namespace_book_store.html',1,'']]],
  ['buttonlog_18',['ButtonLog',['../class_book_store_1_1_button_log.html',1,'BookStore']]]
];
