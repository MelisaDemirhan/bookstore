var searchData=
[
  ['magazine_46',['Magazine',['../class_book_store_1_1_magazine.html',1,'BookStore.Magazine'],['../class_book_store_1_1_magazine.html#a2866ffec8be4d9f3532390aece802d3a',1,'BookStore.Magazine.Magazine()']]],
  ['magazinelist_47',['MagazineList',['../class_book_store_1_1_sql_queries.html#af5bc30c3cbf2b389fc4dc0f7fc764fcf',1,'BookStore::SqlQueries']]],
  ['magazinemodel_48',['MagazineModel',['../class_book_store_1_1_magazine_model.html',1,'BookStore.MagazineModel'],['../class_book_store_1_1_magazine_model.html#ac99824db5002c1d3e09d82336dd23ad6',1,'BookStore.MagazineModel.MagazineModel(string name, string id, int price, string picture, string issue, string magazineType)'],['../class_book_store_1_1_magazine_model.html#a059cf7baae0d150e17735735c1fb854b',1,'BookStore.MagazineModel.MagazineModel(string _name, int _price)'],['../class_book_store_1_1_magazine_model.html#a523fcda1276b0723ac2230108c812866',1,'BookStore.MagazineModel.MagazineModel(string _name, int _price, string _ID, string _picture)']]],
  ['magazines_49',['Magazines',['../class_book_store_1_1_magazines.html',1,'BookStore']]],
  ['musiccd_50',['MusicCd',['../class_book_store_1_1_music_cd.html',1,'BookStore.MusicCd'],['../class_book_store_1_1_music_cd.html#ac3ccac63c08e01ccce1a5a9f60ed3861',1,'BookStore.MusicCd.MusicCd()']]],
  ['musiccdlist_51',['MusicCDList',['../class_book_store_1_1_sql_queries.html#a108e5862e027ef6531c1c9dc93e054b1',1,'BookStore::SqlQueries']]],
  ['musiccdmodel_52',['MusicCDModel',['../class_book_store_1_1_music_c_d_model.html',1,'BookStore.MusicCDModel'],['../class_book_store_1_1_music_c_d_model.html#a9aeba25766d8ab23b6beb32906ebf4c6',1,'BookStore.MusicCDModel.MusicCDModel(string name, string id, int price, string picture, string singer, string musicType)'],['../class_book_store_1_1_music_c_d_model.html#af00b8ed0d048720cc85cecf984e087fe',1,'BookStore.MusicCDModel.MusicCDModel(string _name, int _price, string _ID, string _coverPage)']]],
  ['musiccds_53',['MusicCds',['../class_book_store_1_1_music_cds.html',1,'BookStore']]]
];
