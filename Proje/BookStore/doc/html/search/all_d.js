var searchData=
[
  ['updatebook_76',['UpdateBook',['../class_book_store_1_1_sql_queries.html#adb33cfd66b1dcc8756fd2604d33a2627',1,'BookStore::SqlQueries']]],
  ['updatemagazine_77',['UpdateMagazine',['../class_book_store_1_1_sql_queries.html#aa7790d23c64c2e19d378a1e1d2b99533',1,'BookStore::SqlQueries']]],
  ['updatemusiccd_78',['UpdateMusicCD',['../class_book_store_1_1_sql_queries.html#a1cd6f5b4a35f190b18d2a756ff822e63',1,'BookStore::SqlQueries']]],
  ['uploadallbooksinfos_79',['UploadAllBooksInfos',['../class_book_store_1_1_categories.html#a14b3ac5bbfc065b8bf036caa72623f64',1,'BookStore::Categories']]],
  ['uploadallmagazinesinfos_80',['UploadAllMagazinesInfos',['../class_book_store_1_1_categories.html#a0fe3be6a6d8ba2966d514759c25a1036',1,'BookStore::Categories']]],
  ['uploadallmusicsinfos_81',['UploadAllMusicsInfos',['../class_book_store_1_1_categories.html#a3a243c369980770541ce98ac82e66e54',1,'BookStore::Categories']]],
  ['uploadbook_82',['UploadBook',['../class_book_store_1_1_admin_book.html#ad8d321d56e8b87608a6d8e94f2c2f324',1,'BookStore::AdminBook']]],
  ['uploadbooksinfos_83',['UploadBooksInfos',['../class_book_store_1_1_categories.html#a894203c8a77cb00962d09aae89f350b8',1,'BookStore::Categories']]],
  ['uploadcd_84',['UploadCD',['../class_book_store_1_1_admin_music_c_d.html#a57cd2e5a086f45fd928e196164ebc473',1,'BookStore::AdminMusicCD']]],
  ['uploadmagazine_85',['UploadMagazine',['../class_book_store_1_1_admin_magazine.html#a6413fb9014254473e7a1a8c6b06746db',1,'BookStore::AdminMagazine']]],
  ['uploadmagazinesinfos_86',['UploadMagazinesInfos',['../class_book_store_1_1_categories.html#a7c23320e8d3d1bb4c28db7d83930f1e9',1,'BookStore::Categories']]],
  ['uploadmusicsinfos_87',['UploadMusicsInfos',['../class_book_store_1_1_categories.html#a842c83aacbb764687e9499ce1d33e51f',1,'BookStore::Categories']]],
  ['uploadshoplisttocart_88',['UploadShopListToCart',['../class_book_store_1_1_shopping_crts.html#aeb795da75d572e269bdbc62076d989e6',1,'BookStore::ShoppingCrts']]],
  ['userauthentication_89',['UserAuthentication',['../class_book_store_1_1_user_authentication.html',1,'BookStore']]],
  ['userregisteration_90',['UserRegisteration',['../class_book_store_1_1_user_registeration.html',1,'BookStore']]]
];
