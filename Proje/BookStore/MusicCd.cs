﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Merve Katı
*  @number  : 152120171025
*  @mail    : mervekati350@gmail.com
*  @date    : 28.05.2021
*  @brief   : this class for magazine infos
*/
namespace BookStore
{
    public partial class MusicCd : UserControl
    {
        string ID;
        string picB;
        SqlQueries sqlQueries = new SqlQueries();
        /// <summary>
        /// This parameter is constructor.
        /// </summary>
        /// <param name="musicCD">This parameter is musicCD.</param>
        public MusicCd(MusicCDModel musicCD)
        {
            this.Size = new Size(284, 453);
            InitializeComponent();
            labelMusicCDName.Text = musicCD.name;
            labelSinger.Text = musicCD.singer;
            labelType.Text = musicCD.type.ToString();
            labelMusicCDPrice.Text = musicCD.price.ToString();
            ID = musicCD.id;
            img.Load(musicCD.picture);          
            picB = musicCD.picture;
        }
        /// <summary>
        /// This function adds the selected product to customer's cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddCD_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("AddMusicCd");
            labelProductNumber.Text = (int.Parse(labelProductNumber.Text) + 1).ToString();
            ItemToPurchase itemTP = new ItemToPurchase();
            itemTP.Product = new MusicCDModel(labelMusicCDName.Text, int.Parse(labelMusicCDPrice.Text), ID, picB);
            itemTP.Quantity = 1;
            ShoppingCrt.cart.addProduct(itemTP);
            sqlQueries.SaveCartProduct(itemTP,"CD");
        }
        /// <summary>
        /// This function subtracts the selected product from customer's cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubtractCD_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("SubstractMusicCd");
            if (int.Parse(labelProductNumber.Text) > 0)
            {
                labelProductNumber.Text = (int.Parse(labelProductNumber.Text) - 1).ToString();
                ItemToPurchase itemTP = new ItemToPurchase();
                itemTP.Product = new MusicCDModel(labelMusicCDName.Text, int.Parse(labelMusicCDPrice.Text));
                itemTP.Quantity = 1;
                ShoppingCrt.cart.removeProduct(itemTP);
                sqlQueries.DeleteCartProductList(itemTP, 1);
            }
        }
    }
}

