﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
/**
 *@author : Özge Katırcı
 *@number : 152120181021
 *@mail   : ozgekatirci0@gmail.com
 *@date   : 5-29-2021
 *@brief  : this class for shopping carts operations.
 */

namespace BookStore
{
    public partial class ShoppingCrts : UserControl
    {
        /// <summary>
        /// This parameter is constructor.
        /// </summary>
        public ShoppingCrts()
        {
            InitializeComponent();
            labelPaymentAmount.Text = ShoppingCrt.cart.PaymentAmount.ToString();
            sqlQueries.CartProductList();
            DataTable a = sqlQueries.getCartProduct();
            for (int i = 0; i < a.Rows.Count; i++)
            {
                if (a.Rows[i][6].ToString() == "CD")
                {
                    ItemToPurchase itemTP = new ItemToPurchase();
                    itemTP.Product = new MusicCDModel(a.Rows[i][1].ToString(), Int32.Parse(a.Rows[i][2].ToString()), a.Rows[i][0].ToString(), a.Rows[i][3].ToString());
                    itemTP.Quantity = Int32.Parse(a.Rows[i][4].ToString());
                    ShoppingCrt.cart.addProduct(itemTP);
                }
                if (a.Rows[i][6].ToString() == "Book")
                {
                    ItemToPurchase itemTP = new ItemToPurchase();
                    itemTP.Product = new BookModel(a.Rows[i][1].ToString(), Int32.Parse(a.Rows[i][2].ToString()), a.Rows[i][0].ToString(), a.Rows[i][3].ToString());
                    itemTP.Quantity = Int32.Parse(a.Rows[i][4].ToString());
                    ShoppingCrt.cart.addProduct(itemTP);
                }
                if (a.Rows[i][6].ToString() == "Magazine")
                {
                    ItemToPurchase itemTP = new ItemToPurchase();
                    itemTP.Product = new MagazineModel(a.Rows[i][1].ToString(), Int32.Parse(a.Rows[i][2].ToString()), a.Rows[i][0].ToString(), a.Rows[i][3].ToString());
                    itemTP.Quantity = Int32.Parse(a.Rows[i][4].ToString());
                    ShoppingCrt.cart.addProduct(itemTP);
                }

            }

        }
        SqlQueries sqlQueries = new SqlQueries();
        /// <summary>
        /// This function upload shop list to cart. 
        /// </summary>
    
        public static ShoppingCrts UploadShopListToCart()
        {
            ShoppingCrts items = new ShoppingCrts();
            foreach (ItemToPurchase item in ShoppingCart.ItemsToPurchase)
            {
                items.flowLayoutPanelCrts.Controls.Add(new ShoppingCrt(item));
            }
            items.Dock = DockStyle.Fill;
            return items;
        }
        /// <summary>
        /// This function deletes all products from cart. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteCart_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("DeleteShoppingCrts");
            ShoppingCrt.cart.cancelOrder();
            sqlQueries.DeleteAllCartProductList();
            flowLayoutPanelCrts.Controls.Clear();
            labelPaymentAmount.Text = "0";
        }
        /// <summary>
        /// This function to confirm cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfrimCart_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("ConfrimCart");
            StartForm anaform = (StartForm)Application.OpenForms["StartForm"];
            CartConfirm a = new CartConfirm();
            sqlQueries.DeleteAllCartProductList();
            a.Dock = DockStyle.Fill;
            anaform.paneluser.Controls.Add(a);
            anaform.paneluser.Controls["CartConfirm"].BringToFront();
            anaform.bttnexitStart.BringToFront();
        }
    }
}
