﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 30.05.2021 
 *@brief  : includes products to be sold and their quantities 
 */
namespace BookStore
{
    public class ItemToPurchase
    {
        private Product product;
        private int quantity;
        /// <summary>
        /// This functions are getters and setters.
        /// </summary>
        public Product Product { get => product; set => product = value; }
        public int Quantity { get => quantity; set => quantity = value; }
    }
}
