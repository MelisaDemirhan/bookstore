﻿
namespace BookStore
{
    partial class AdminControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonMagazine = new System.Windows.Forms.Button();
            this.buttonCD = new System.Windows.Forms.Button();
            this.button_AdminUser = new System.Windows.Forms.Button();
            this.buttonBook = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonCD);
            this.panel1.Controls.Add(this.button_AdminUser);
            this.panel1.Controls.Add(this.buttonMagazine);
            this.panel1.Controls.Add(this.buttonBook);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(965, 112);
            this.panel1.TabIndex = 0;
            // 
            // buttonMagazine
            // 
            this.buttonMagazine.BackColor = System.Drawing.Color.RosyBrown;
            this.buttonMagazine.FlatAppearance.BorderSize = 0;
            this.buttonMagazine.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMagazine.Location = new System.Drawing.Point(489, 13);
            this.buttonMagazine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMagazine.Name = "buttonMagazine";
            this.buttonMagazine.Size = new System.Drawing.Size(212, 81);
            this.buttonMagazine.TabIndex = 4;
            this.buttonMagazine.Text = "MAGAZİNE";
            this.buttonMagazine.UseVisualStyleBackColor = false;
            this.buttonMagazine.Click += new System.EventHandler(this.buttonMagazine_Click);
            // 
            // buttonCD
            // 
            this.buttonCD.BackColor = System.Drawing.Color.RosyBrown;
            this.buttonCD.FlatAppearance.BorderSize = 0;
            this.buttonCD.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCD.Location = new System.Drawing.Point(732, 13);
            this.buttonCD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonCD.Name = "buttonCD";
            this.buttonCD.Size = new System.Drawing.Size(212, 81);
            this.buttonCD.TabIndex = 3;
            this.buttonCD.Text = "MUSIC CD";
            this.buttonCD.UseVisualStyleBackColor = false;
            this.buttonCD.Click += new System.EventHandler(this.buttonCD_Click);
            // 
            // button_AdminUser
            // 
            this.button_AdminUser.BackColor = System.Drawing.Color.RosyBrown;
            this.button_AdminUser.FlatAppearance.BorderSize = 0;
            this.button_AdminUser.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_AdminUser.Location = new System.Drawing.Point(3, 13);
            this.button_AdminUser.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_AdminUser.Name = "button_AdminUser";
            this.button_AdminUser.Size = new System.Drawing.Size(212, 81);
            this.button_AdminUser.TabIndex = 1;
            this.button_AdminUser.Text = "USER";
            this.button_AdminUser.UseVisualStyleBackColor = false;
            this.button_AdminUser.Click += new System.EventHandler(this.button_AdminUser_Click);
            // 
            // buttonBook
            // 
            this.buttonBook.BackColor = System.Drawing.Color.RosyBrown;
            this.buttonBook.FlatAppearance.BorderSize = 0;
            this.buttonBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonBook.Location = new System.Drawing.Point(246, 13);
            this.buttonBook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonBook.Name = "buttonBook";
            this.buttonBook.Size = new System.Drawing.Size(212, 81);
            this.buttonBook.TabIndex = 2;
            this.buttonBook.Text = "BOOK";
            this.buttonBook.UseVisualStyleBackColor = false;
            this.buttonBook.Click += new System.EventHandler(this.buttonBook_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MistyRose;
            this.panel2.Location = new System.Drawing.Point(0, 116);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(962, 562);
            this.panel2.TabIndex = 1;
            // 
            // AdminControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AdminControl";
            this.Size = new System.Drawing.Size(965, 678);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_AdminUser;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonCD;
        private System.Windows.Forms.Button buttonMagazine;
        private System.Windows.Forms.Button buttonBook;
    }
}
