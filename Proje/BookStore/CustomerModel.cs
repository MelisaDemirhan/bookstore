﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
/**
 *@author : Muhammed Suwaneh
 *@number : 152120181098
 *@mail   : suwanehmuhammed1@gmail.com 
 *@date   : 5-28-2021 
 *@brief  : customer's model - has all the customer properties with their methods
 */
namespace BookStore
{

    public class CustomerModel
    {
        private string userName;

        private string userEmail;

        private string userPassword;

        private string userAddress;

        private string userID;

        private bool isAdmin;

        private string name;
        SqlQueries sqlQueries = new SqlQueries();

        public string UserName { get => userName; set => userName = value; }
        public string UserEmail { get => userEmail; set => userEmail = value; }
        public string UserPassword { get => userPassword; set => userPassword = value; }
        public string UserAddress { get => userAddress; set => userAddress = value; }
        public string UserID { get => userID; set => userID = value; }
        public bool IsAdmin { get => isAdmin; set => isAdmin = value; }
        public string Name { get => name; set => name = value; }

        /// <summary>
        /// initializes private variables 
        /// </summary>
        public CustomerModel(string username, string email, string password, string address, string id, bool isAdmin, string name)
        {
            userName = username;
            userEmail = email;
            userPassword = password;
            userAddress = address;
            userID = id;
            this.isAdmin = isAdmin;
            this.name = name;
        }

        /// <summary>
        /// singleton implementation
        /// </summary>
        /// <returns></returns>
        public static CustomerModel GetCustomer(string username, string email, string password, string address, string id, bool admin, string name)
        {
            return new CustomerModel(username, email, password, address, id, admin, name);
        }
        
        /// <summary>
        /// saves customer to the database 
        /// </summary>
        public void saveCustomer()
        {
            sqlQueries.AddCustomer(this);
        }

        /// <summary>
        /// prints customer's detail
        /// </summary>
        public string printCustomerDetails()
        {
            return String.Format($"{userID}- {name} - {userName} - {userEmail} - {userAddress}");
        }

        /// <summary>
        /// prints users pruchases
        /// </summary>
        public void printCustomerPurchases()
        {
            throw new NotImplementedException();
        }
    }
}
