﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for adding  books to database, updating books from databsse, and deleting books from database. 
 */
namespace BookStore
{
    public partial class AdminBook : UserControl
    {
        public AdminBook()
        {
            InitializeComponent();
        }
        string imgx;
        string namex, idx, isbnx, authorx, publisherx;
        int pricex, pagex;
        public static int selectedIndex = -1;
        public static int index = -1;
        SqlQueries sq1 = new SqlQueries();
        /// <summary> Function to make operations when form load. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminBook_Load(object sender, EventArgs e)
        {
            try
            {
                List<Product> BKList = UploadBook();
                ListBooks(BKList);
                dataGridView1.Show();
                dataGridView1.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to list books. 
        /// </summary>
        /// <param name="books"></param>
       
        private void ListBooks(List<Product> _books)
        {
            try
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < _books.Count; i++)
                {
                    BookModel b = new BookModel(namex, idx, pricex, imgx, isbnx, authorx, publisherx, pagex);
                    b = (BookModel)_books[i];
                    dataGridView1.Rows.Add();

                    dataGridView1.Rows[i].Cells[0].Value = b.id;
                    dataGridView1.Rows[i].Cells[1].Value = b.name;
                    dataGridView1.Rows[i].Cells[2].Value = b.Author;
                    dataGridView1.Rows[i].Cells[3].Value = b.Publisher;
                    dataGridView1.Rows[i].Cells[4].Value = b.ISBNumber;
                    dataGridView1.Rows[i].Cells[5].Value = b.Page;
                    dataGridView1.Rows[i].Cells[6].Value = b.price;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to add new books to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Add");
            try
            {
                bool exist = false;
                List<Product> BKList = UploadBook();


                if (exist == false)
                {
                    if (textBoxName.Text != "" && textBoxAuthor.Text != "" && textBoxISBN.Text != "" && textBoxPrice.Text != "" && textBoxPublisher.Text != "" && pictureBox1.Image != null && textBoxPages.Text != "" && textBoxURL.Text != "")
                    {
                        string name = textBoxName.Text;
                        string price = textBoxPrice.Text;
                        string isbn = textBoxISBN.Text;
                        string author = textBoxAuthor.Text;
                        string publisher = textBoxPublisher.Text;
                        string page = textBoxPages.Text;
                        string image = textBoxURL.Text;
                        BookModel book = new BookModel(name, "", Int32.Parse(price), image, isbn, author, publisher, Int32.Parse(page));
                        sq1.AddBook(book);
                        ListBooks(BKList);
                    }
                    else
                    {
                        MessageBox.Show("Please! Fill all fields!");
                    }
                }
                ClearTextBox();
                AdminBook_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        /// <summary> Function to update new books from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Update");

            try
            {
                selectedIndex = dataGridView1.CurrentCell.RowIndex;
                string name = textBoxName.Text;
                string price = textBoxPrice.Text;
                string isbn = textBoxISBN.Text;
                string author = textBoxAuthor.Text;
                string publisher = textBoxPublisher.Text;
                string page = textBoxPages.Text;
                string image = textBoxURL.Text;
                string id = dataGridView1.Rows[selectedIndex].Cells[0].Value.ToString();

                BookModel book = new BookModel(name, id, Int32.Parse(price), image, isbn, author, publisher, Int32.Parse(page));
                sq1.UpdateBook(book);
                List<Product> BKList = UploadBook();
                if (BKList.Count != 0)
                {
                    BKList.Clear();
                }
                dataGridView1.Show();
                selectedIndex = -1;
                ListBooks(BKList);
                AdminBook_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        /// <summary> Function to delete  books from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Delete");
            try
            {
                dataGridView1.FirstDisplayedCell = null;
                dataGridView1.ClearSelection();
                List<Product> BKList = UploadBook();
                if (BKList.Count != 0)
                {
                    for (int i = 0; i < BKList.Count; i++)
                    {
                        selectedIndex = dataGridView1.CurrentCell.RowIndex;
                        string name = dataGridView1.Rows[selectedIndex].Cells[1].Value.ToString();
                        string price = dataGridView1.Rows[selectedIndex].Cells[6].Value.ToString();
                        string id = dataGridView1.Rows[selectedIndex].Cells[0].Value.ToString();
                        string isbn = dataGridView1.Rows[selectedIndex].Cells[4].Value.ToString();
                        string author = dataGridView1.Rows[selectedIndex].Cells[2].Value.ToString();
                        string publisher = dataGridView1.Rows[selectedIndex].Cells[3].Value.ToString();
                        string page = dataGridView1.Rows[selectedIndex].Cells[5].Value.ToString();

                        BookModel book = new BookModel(name, id, Int32.Parse(price), "", isbn, author, publisher, Int32.Parse(page));
                        sq1.DeleteBook(book);
                        if (BKList.Count != 0)
                        {
                            BKList.Clear();
                        }
                        dataGridView1.Rows.RemoveAt(selectedIndex);
                        selectedIndex = -1;
                        ListBooks(BKList);
                    }
                }
                else
                {
                    MessageBox.Show("Book is Empty!");
                    buttonDelete.Enabled = false;
                    buttonUpdate.Enabled = false;
                }
                ClearTextBox();
                AdminBook_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        /// <summary> Function to clear database..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }
        /// <summary> Function to get infos selected index on the datagridview..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                List<Product> BKList = UploadBook();
                if (BKList.Count != 0)
                {
                    index = e.RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    textBoxName.Text = row.Cells[1].Value.ToString();
                    textBoxAuthor.Text = row.Cells[2].Value.ToString();
                    textBoxPublisher.Text = row.Cells[3].Value.ToString();
                    textBoxISBN.Text = row.Cells[4].Value.ToString();
                    textBoxPages.Text = row.Cells[5].Value.ToString();
                    textBoxPrice.Text = row.Cells[6].Value.ToString();

                    foreach (var item in BKList)
                    {
                        if (item.id == row.Cells[0].Value.ToString())
                        {
                            textBoxURL.Text = item.picture;
                            pictureBox1.Load(item.picture);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Book is empty!");
                    buttonDelete.Enabled = false;
                    buttonUpdate.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to load new picture for books.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Load");
            try
            {
                pictureBox1.Load(textBoxURL.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        ///Function to clear textboxes.
        /// </summary>
        private void ClearTextBox()
        {
            textBoxAuthor.Text = "";
            textBoxISBN.Text = "";
            textBoxName.Text = "";
            textBoxPages.Text = "";
            textBoxPrice.Text = "";
            textBoxPublisher.Text = "";
            textBoxURL.Text = "";
            pictureBox1.Image = null;
        }
        /// <summary>
        ///Function to get book infos from database.
        /// </summary>
        public static List<Product> UploadBook()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.BookList();
            return sq1.getBookList();
        }

    }
}
