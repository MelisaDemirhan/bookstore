﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
/**
 *@author : Muhammed Suwaneh
 *@number : 152120181098
 *@mail   : suwanehmuhammed1@gmail.com 
 *@date   : 5-29-2021
 *@brief  : authenticates users 
 */

namespace BookStore
{
    public partial class UserAuthentication : Form
    {


        private bool ErrorExist;
        SqlQueries sqlQueries = new SqlQueries();
        public UserAuthentication()
        {
            InitializeComponent();

            // empty errors
            usernameFieldrrorLabel.Text = string.Empty;
            passwordFieldError.Text = string.Empty;
            authenticationFeedbackLabel.Text = string.Empty;
        }

        /// <summary>
        /// Start authentication process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void signInButton_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("SignIn");
            // check if there empty fields
            checkForEmptyFields();

            // check if user exist 
            if (!ErrorExist)
            {
                // perform authentication 

                string password = Helper.EncryptData(passwordField.Text);

                ActiveUser.getInstance().Customer = sqlQueries.getActiveUser(usernameField.Text, password);

                // throw feedback
                if (ActiveUser.getInstance().Customer != null)
                {

                    // disable links and show new success login
                    authenticationFeedbackLabel.Text = "Sign in was successful";
                    authenticationFeedbackLabel.ForeColor = Color.Green;
                    signInButton.Enabled = false;                  
                  
                    if (ActiveUser.getInstance().Customer.IsAdmin)
                    {
                        StartForm anaform = (StartForm)Application.OpenForms["StartForm"];
                        anaform.buttonAdmin.Visible = true;
                    }

                    DateTime timeStamp = DateTime.Now;
                    // redirect user to Start Form
                    MessageBox.Show($"User Data - \n {ActiveUser.getInstance().Customer.printCustomerDetails()} - Current Time  {timeStamp.ToString("hh:mm tt")}");
                    if (ActiveUser.getInstance().Customer.IsAdmin)
                        StartForm.staticbutton5.Visible = true;
                    //bring to front account form that have user's information
                    Form form = Application.OpenForms["StartForm"];
                    form = Application.OpenForms["StartForm"] as StartForm;
                    Panel panel = form.Controls["paneluser"] as Panel;
                    Account us = new Account();
                    us.Dock = DockStyle.Fill;
                    panel.Controls.Add(us);
                    panel.Controls["Account"].BringToFront();
                    StartForm.exitbutton.BringToFront();
                    StartForm.staticbutton.Text = "Account";
                    StartForm.staticbutton1.Visible = false;
                    StartForm.staticbutton2.Visible = true;
                    StartForm.staticbutton3.Visible = true;
                    StartForm.staticbutton6.Visible = true;
                    Button loginadvice = new Button();
                    loginadvice.Width = 165;
                    loginadvice.Height = 44;
                    loginadvice.Location = new Point(8, 262);
                    loginadvice.Text = "Advice";
                    loginadvice.Font = new Font("Century Gothic", 11, FontStyle.Bold);
                    loginadvice.FlatStyle = FlatStyle.Flat;
                    loginadvice.FlatAppearance.BorderSize = 0;
                    loginadvice.ForeColor = System.Drawing.Color.FromArgb(89, 98, 96);
                    loginadvice.Click += StartForm.buttonadvice;
                    Panel panel1 = form.Controls["panel2"] as Panel;
                    panel1.Controls.Add(loginadvice);
                }

                else
                {
                    Helper.ShowErrors(authenticationFeedbackLabel, "Authentication failed. Please check credentials");
                }
            }
        }


        /// <summary>
        /// throws errors on empty fields
        /// </summary>
        private void checkForEmptyFields()
        {
            // check for empty username field 
            if (usernameField.Text == string.Empty)
            {
                ErrorExist = true;
                Helper.ShowErrors(usernameFieldrrorLabel, "Username is required");
            }

            // check for empty password field 
            if (passwordField.Text == string.Empty)
            {
                ErrorExist = true;
                Helper.ShowErrors(passwordFieldError, "Password is required");
            }

            // error does not exist
            else
            {
                ErrorExist = false;
            }
        }
    }
}
