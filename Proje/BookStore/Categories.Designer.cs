﻿
namespace BookStore
{
    partial class Categories
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categories));
            this.buttonMagazine = new System.Windows.Forms.Button();
            this.buttonBooks = new System.Windows.Forms.Button();
            this.buttonMusicCDs = new System.Windows.Forms.Button();
            this.panelCat = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // buttonMagazine
            // 
            this.buttonMagazine.BackColor = System.Drawing.Color.Transparent;
            this.buttonMagazine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonMagazine.BackgroundImage")));
            this.buttonMagazine.FlatAppearance.BorderSize = 0;
            this.buttonMagazine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMagazine.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonMagazine.Location = new System.Drawing.Point(23, 27);
            this.buttonMagazine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMagazine.Name = "buttonMagazine";
            this.buttonMagazine.Size = new System.Drawing.Size(267, 91);
            this.buttonMagazine.TabIndex = 2;
            this.buttonMagazine.UseVisualStyleBackColor = false;
            this.buttonMagazine.Click += new System.EventHandler(this.buttonMagazine_Click);
            // 
            // buttonBooks
            // 
            this.buttonBooks.BackColor = System.Drawing.Color.Transparent;
            this.buttonBooks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonBooks.BackgroundImage")));
            this.buttonBooks.FlatAppearance.BorderSize = 0;
            this.buttonBooks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBooks.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonBooks.Location = new System.Drawing.Point(358, 21);
            this.buttonBooks.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonBooks.Name = "buttonBooks";
            this.buttonBooks.Size = new System.Drawing.Size(255, 97);
            this.buttonBooks.TabIndex = 3;
            this.buttonBooks.UseVisualStyleBackColor = false;
            this.buttonBooks.Click += new System.EventHandler(this.buttonBooks_Click);
            // 
            // buttonMusicCDs
            // 
            this.buttonMusicCDs.BackColor = System.Drawing.Color.Transparent;
            this.buttonMusicCDs.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonMusicCDs.BackgroundImage")));
            this.buttonMusicCDs.FlatAppearance.BorderSize = 0;
            this.buttonMusicCDs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMusicCDs.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttonMusicCDs.Location = new System.Drawing.Point(677, 22);
            this.buttonMusicCDs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonMusicCDs.Name = "buttonMusicCDs";
            this.buttonMusicCDs.Size = new System.Drawing.Size(253, 95);
            this.buttonMusicCDs.TabIndex = 3;
            this.buttonMusicCDs.UseVisualStyleBackColor = false;
            this.buttonMusicCDs.Click += new System.EventHandler(this.buttonMusicCDs_Click);
            // 
            // panelCat
            // 
            this.panelCat.BackColor = System.Drawing.Color.Transparent;
            this.panelCat.Location = new System.Drawing.Point(0, 136);
            this.panelCat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelCat.Name = "panelCat";
            this.panelCat.Size = new System.Drawing.Size(962, 540);
            this.panelCat.TabIndex = 4;
            // 
            // Categories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.Controls.Add(this.buttonBooks);
            this.Controls.Add(this.panelCat);
            this.Controls.Add(this.buttonMusicCDs);
            this.Controls.Add(this.buttonMagazine);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Categories";
            this.Size = new System.Drawing.Size(965, 678);
            this.Load += new System.EventHandler(this.Categories_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonMagazine;
        private System.Windows.Forms.Button buttonBooks;
        private System.Windows.Forms.Button buttonMusicCDs;
        private System.Windows.Forms.Panel panelCat;
    }
}
