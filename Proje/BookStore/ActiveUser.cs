﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 30.05.2021 
 *@brief  : this class created for active user.
 */



namespace BookStore
{
    public class ActiveUser
    {
        private CustomerModel customer;
        private static ActiveUser activeUser;
        /// <summary>
        ///  getter and setter.
        /// </summary>
        public CustomerModel Customer { get => customer; set => customer = value; }
        /// <summary>
        ///Singleton design pattern used for only one active user.
        /// </summary>
        public static ActiveUser getInstance()
        {
            if (activeUser == null)
            {
                activeUser = new ActiveUser();
            }
            return activeUser;
        }
    }
}
