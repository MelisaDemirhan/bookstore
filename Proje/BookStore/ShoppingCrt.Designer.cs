﻿
namespace BookStore
{
    partial class ShoppingCrt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelItem = new System.Windows.Forms.Button();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.lblHeaderUnitPrice = new System.Windows.Forms.Label();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.labelProductQuantity = new System.Windows.Forms.Label();
            this.lblHeaderTotalPrice = new System.Windows.Forms.Label();
            this.lblHeaderQuantity = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.picCoverPage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picCoverPage)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelItem
            // 
            this.btnCancelItem.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnCancelItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelItem.Location = new System.Drawing.Point(4, 442);
            this.btnCancelItem.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelItem.Name = "btnCancelItem";
            this.btnCancelItem.Size = new System.Drawing.Size(165, 33);
            this.btnCancelItem.TabIndex = 59;
            this.btnCancelItem.Text = "Delete Item";
            this.btnCancelItem.UseVisualStyleBackColor = false;
            this.btnCancelItem.Click += new System.EventHandler(this.btnCancelItem_Click);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblUnitPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUnitPrice.Location = new System.Drawing.Point(62, 339);
            this.lblUnitPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(39, 33);
            this.lblUnitPrice.TabIndex = 58;
            this.lblUnitPrice.Text = "$0";
            // 
            // lblHeaderUnitPrice
            // 
            this.lblHeaderUnitPrice.AutoSize = true;
            this.lblHeaderUnitPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderUnitPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeaderUnitPrice.Location = new System.Drawing.Point(35, 306);
            this.lblHeaderUnitPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderUnitPrice.Name = "lblHeaderUnitPrice";
            this.lblHeaderUnitPrice.Size = new System.Drawing.Size(113, 33);
            this.lblHeaderUnitPrice.TabIndex = 57;
            this.lblHeaderUnitPrice.Text = "Unit Price";
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.labelTotalPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelTotalPrice.Location = new System.Drawing.Point(62, 405);
            this.labelTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(39, 33);
            this.labelTotalPrice.TabIndex = 56;
            this.labelTotalPrice.Text = "$0";
            // 
            // labelProductQuantity
            // 
            this.labelProductQuantity.AutoSize = true;
            this.labelProductQuantity.BackColor = System.Drawing.Color.Transparent;
            this.labelProductQuantity.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelProductQuantity.ForeColor = System.Drawing.Color.Black;
            this.labelProductQuantity.Location = new System.Drawing.Point(62, 227);
            this.labelProductQuantity.Name = "labelProductQuantity";
            this.labelProductQuantity.Size = new System.Drawing.Size(29, 33);
            this.labelProductQuantity.TabIndex = 55;
            this.labelProductQuantity.Text = "0";
            // 
            // lblHeaderTotalPrice
            // 
            this.lblHeaderTotalPrice.AutoSize = true;
            this.lblHeaderTotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderTotalPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeaderTotalPrice.Location = new System.Drawing.Point(22, 372);
            this.lblHeaderTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderTotalPrice.Name = "lblHeaderTotalPrice";
            this.lblHeaderTotalPrice.Size = new System.Drawing.Size(120, 33);
            this.lblHeaderTotalPrice.TabIndex = 52;
            this.lblHeaderTotalPrice.Text = "Total Price";
            // 
            // lblHeaderQuantity
            // 
            this.lblHeaderQuantity.AutoSize = true;
            this.lblHeaderQuantity.BackColor = System.Drawing.Color.Transparent;
            this.lblHeaderQuantity.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblHeaderQuantity.Location = new System.Drawing.Point(34, 194);
            this.lblHeaderQuantity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeaderQuantity.Name = "lblHeaderQuantity";
            this.lblHeaderQuantity.Size = new System.Drawing.Size(100, 33);
            this.lblHeaderQuantity.TabIndex = 51;
            this.lblHeaderQuantity.Text = "Quantity";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblName.Location = new System.Drawing.Point(35, 150);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 33);
            this.lblName.TabIndex = 69;
            // 
            // picCoverPage
            // 
            this.picCoverPage.Location = new System.Drawing.Point(28, 14);
            this.picCoverPage.Margin = new System.Windows.Forms.Padding(4);
            this.picCoverPage.Name = "picCoverPage";
            this.picCoverPage.Size = new System.Drawing.Size(141, 122);
            this.picCoverPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCoverPage.TabIndex = 60;
            this.picCoverPage.TabStop = false;
            // 
            // ShoppingCrt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.picCoverPage);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnCancelItem);
            this.Controls.Add(this.lblUnitPrice);
            this.Controls.Add(this.lblHeaderUnitPrice);
            this.Controls.Add(this.labelTotalPrice);
            this.Controls.Add(this.labelProductQuantity);
            this.Controls.Add(this.lblHeaderTotalPrice);
            this.Controls.Add(this.lblHeaderQuantity);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ShoppingCrt";
            this.Size = new System.Drawing.Size(195, 567);
            ((System.ComponentModel.ISupportInitialize)(this.picCoverPage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelItem;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.Label lblHeaderUnitPrice;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.Label labelProductQuantity;
        private System.Windows.Forms.Label lblHeaderTotalPrice;
        private System.Windows.Forms.Label lblHeaderQuantity;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.PictureBox picCoverPage;
    }
}
