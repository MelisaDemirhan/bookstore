﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Merve Katı
*  @number  : 152120171025
*  @mail    : mervekati350@gmail.com
*  @date    : 27.05.2021
*  @brief   : abstract class for all products 
*/
namespace BookStore
{
    public abstract class Product
    {
        //the product name,id,price and picture features
        public string name { get; set; }
        public string id { get; set; }
        public int price { get; set; }
        public string picture { get; set; }
        /// <summary>
        /// This function is abstract print properties.
        /// </summary>
        public abstract string printProperties();
    }
}
