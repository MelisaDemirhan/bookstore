﻿
namespace BookStore
{
    partial class UserRegisteration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserRegisteration));
            this.signUpRedirectLink = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.registerationFeedbackLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.usernameField = new System.Windows.Forms.TextBox();
            this.password2Field = new System.Windows.Forms.TextBox();
            this.password2ErrorLabel = new System.Windows.Forms.Label();
            this.password2Label = new System.Windows.Forms.Label();
            this.passwordErrorLabel = new System.Windows.Forms.Label();
            this.passwordField = new System.Windows.Forms.TextBox();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.emailFieldError = new System.Windows.Forms.Label();
            this.emailField = new System.Windows.Forms.TextBox();
            this.userPasswordLabel = new System.Windows.Forms.Label();
            this.addressField = new System.Windows.Forms.TextBox();
            this.usernameFieldrrorLabel = new System.Windows.Forms.Label();
            this.signUpButton = new System.Windows.Forms.Button();
            this.AddressErrorLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelNameError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // signUpRedirectLink
            // 
            this.signUpRedirectLink.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.signUpRedirectLink.AutoSize = true;
            this.signUpRedirectLink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signUpRedirectLink.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signUpRedirectLink.ForeColor = System.Drawing.Color.Green;
            this.signUpRedirectLink.Location = new System.Drawing.Point(163, 1);
            this.signUpRedirectLink.Margin = new System.Windows.Forms.Padding(0);
            this.signUpRedirectLink.Name = "signUpRedirectLink";
            this.signUpRedirectLink.Size = new System.Drawing.Size(58, 19);
            this.signUpRedirectLink.TabIndex = 8;
            this.signUpRedirectLink.Text = "Sign Up";
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.textBoxName.Location = new System.Drawing.Point(127, 230);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(213, 24);
            this.textBoxName.TabIndex = 6;
            // 
            // registerationFeedbackLabel
            // 
            this.registerationFeedbackLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.registerationFeedbackLabel.AutoSize = true;
            this.registerationFeedbackLabel.BackColor = System.Drawing.Color.Transparent;
            this.registerationFeedbackLabel.Font = new System.Drawing.Font("Microsoft YaHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.registerationFeedbackLabel.ForeColor = System.Drawing.Color.White;
            this.registerationFeedbackLabel.Location = new System.Drawing.Point(386, 543);
            this.registerationFeedbackLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.registerationFeedbackLabel.Name = "registerationFeedbackLabel";
            this.registerationFeedbackLabel.Size = new System.Drawing.Size(158, 23);
            this.registerationFeedbackLabel.TabIndex = 11;
            this.registerationFeedbackLabel.Text = "Sign Up Feedback";
            this.registerationFeedbackLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.usernameLabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.usernameLabel.Location = new System.Drawing.Point(127, 115);
            this.usernameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(77, 18);
            this.usernameLabel.TabIndex = 0;
            this.usernameLabel.Text = "Username";
            // 
            // usernameField
            // 
            this.usernameField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.usernameField.Location = new System.Drawing.Point(127, 137);
            this.usernameField.Margin = new System.Windows.Forms.Padding(4);
            this.usernameField.Name = "usernameField";
            this.usernameField.Size = new System.Drawing.Size(213, 24);
            this.usernameField.TabIndex = 1;
            // 
            // password2Field
            // 
            this.password2Field.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.password2Field.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.password2Field.Location = new System.Drawing.Point(570, 230);
            this.password2Field.Margin = new System.Windows.Forms.Padding(4);
            this.password2Field.MaxLength = 8000;
            this.password2Field.Name = "password2Field";
            this.password2Field.PasswordChar = '*';
            this.password2Field.Size = new System.Drawing.Size(225, 24);
            this.password2Field.TabIndex = 12;
            // 
            // password2ErrorLabel
            // 
            this.password2ErrorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.password2ErrorLabel.AutoSize = true;
            this.password2ErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.password2ErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.password2ErrorLabel.Location = new System.Drawing.Point(570, 258);
            this.password2ErrorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.password2ErrorLabel.Name = "password2ErrorLabel";
            this.password2ErrorLabel.Size = new System.Drawing.Size(40, 18);
            this.password2ErrorLabel.TabIndex = 13;
            this.password2ErrorLabel.Text = "error";
            // 
            // password2Label
            // 
            this.password2Label.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.password2Label.AutoSize = true;
            this.password2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.password2Label.ForeColor = System.Drawing.Color.YellowGreen;
            this.password2Label.Location = new System.Drawing.Point(570, 208);
            this.password2Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.password2Label.Name = "password2Label";
            this.password2Label.Size = new System.Drawing.Size(132, 18);
            this.password2Label.TabIndex = 9;
            this.password2Label.Text = "Confirm Password";
            // 
            // passwordErrorLabel
            // 
            this.passwordErrorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.passwordErrorLabel.AutoSize = true;
            this.passwordErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.passwordErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.passwordErrorLabel.Location = new System.Drawing.Point(570, 165);
            this.passwordErrorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.passwordErrorLabel.Name = "passwordErrorLabel";
            this.passwordErrorLabel.Size = new System.Drawing.Size(40, 18);
            this.passwordErrorLabel.TabIndex = 8;
            this.passwordErrorLabel.Text = "error";
            // 
            // passwordField
            // 
            this.passwordField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.passwordField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.passwordField.Location = new System.Drawing.Point(570, 137);
            this.passwordField.Margin = new System.Windows.Forms.Padding(4);
            this.passwordField.Name = "passwordField";
            this.passwordField.PasswordChar = '*';
            this.passwordField.Size = new System.Drawing.Size(225, 24);
            this.passwordField.TabIndex = 7;
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PasswordLabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.PasswordLabel.Location = new System.Drawing.Point(570, 115);
            this.PasswordLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(75, 18);
            this.PasswordLabel.TabIndex = 6;
            this.PasswordLabel.Text = "Password";
            // 
            // emailFieldError
            // 
            this.emailFieldError.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emailFieldError.AutoSize = true;
            this.emailFieldError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.emailFieldError.ForeColor = System.Drawing.Color.Red;
            this.emailFieldError.Location = new System.Drawing.Point(127, 354);
            this.emailFieldError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.emailFieldError.Name = "emailFieldError";
            this.emailFieldError.Size = new System.Drawing.Size(40, 18);
            this.emailFieldError.TabIndex = 5;
            this.emailFieldError.Text = "error";
            // 
            // emailField
            // 
            this.emailField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.emailField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.emailField.Location = new System.Drawing.Point(127, 326);
            this.emailField.Margin = new System.Windows.Forms.Padding(4);
            this.emailField.Name = "emailField";
            this.emailField.Size = new System.Drawing.Size(225, 24);
            this.emailField.TabIndex = 4;
            // 
            // userPasswordLabel
            // 
            this.userPasswordLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.userPasswordLabel.AutoSize = true;
            this.userPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.userPasswordLabel.ForeColor = System.Drawing.Color.YellowGreen;
            this.userPasswordLabel.Location = new System.Drawing.Point(127, 304);
            this.userPasswordLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.userPasswordLabel.Name = "userPasswordLabel";
            this.userPasswordLabel.Size = new System.Drawing.Size(45, 18);
            this.userPasswordLabel.TabIndex = 3;
            this.userPasswordLabel.Text = "Email";
            // 
            // addressField
            // 
            this.addressField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.addressField.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.addressField.Location = new System.Drawing.Point(570, 323);
            this.addressField.Margin = new System.Windows.Forms.Padding(4);
            this.addressField.Name = "addressField";
            this.addressField.Size = new System.Drawing.Size(225, 24);
            this.addressField.TabIndex = 15;
            // 
            // usernameFieldrrorLabel
            // 
            this.usernameFieldrrorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.usernameFieldrrorLabel.AutoSize = true;
            this.usernameFieldrrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.usernameFieldrrorLabel.ForeColor = System.Drawing.Color.Red;
            this.usernameFieldrrorLabel.Location = new System.Drawing.Point(127, 165);
            this.usernameFieldrrorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.usernameFieldrrorLabel.Name = "usernameFieldrrorLabel";
            this.usernameFieldrrorLabel.Size = new System.Drawing.Size(40, 18);
            this.usernameFieldrrorLabel.TabIndex = 2;
            this.usernameFieldrrorLabel.Text = "error";
            // 
            // signUpButton
            // 
            this.signUpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.signUpButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(98)))), ((int)(((byte)(96)))));
            this.signUpButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signUpButton.FlatAppearance.BorderSize = 0;
            this.signUpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signUpButton.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signUpButton.ForeColor = System.Drawing.Color.White;
            this.signUpButton.Location = new System.Drawing.Point(121, 468);
            this.signUpButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 4);
            this.signUpButton.Name = "signUpButton";
            this.signUpButton.Size = new System.Drawing.Size(665, 52);
            this.signUpButton.TabIndex = 10;
            this.signUpButton.Text = "Sign Up";
            this.signUpButton.UseVisualStyleBackColor = false;
            this.signUpButton.Click += new System.EventHandler(this.signUpButton_Click);
            // 
            // AddressErrorLabel
            // 
            this.AddressErrorLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.AddressErrorLabel.AutoSize = true;
            this.AddressErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.AddressErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.AddressErrorLabel.Location = new System.Drawing.Point(570, 351);
            this.AddressErrorLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.AddressErrorLabel.Name = "AddressErrorLabel";
            this.AddressErrorLabel.Size = new System.Drawing.Size(40, 18);
            this.AddressErrorLabel.TabIndex = 14;
            this.AddressErrorLabel.Text = "error";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.YellowGreen;
            this.label2.Location = new System.Drawing.Point(570, 301);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Address";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // labelName
            // 
            this.labelName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelName.ForeColor = System.Drawing.Color.YellowGreen;
            this.labelName.Location = new System.Drawing.Point(127, 208);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(48, 18);
            this.labelName.TabIndex = 16;
            this.labelName.Text = "Name";
            // 
            // labelNameError
            // 
            this.labelNameError.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelNameError.AutoSize = true;
            this.labelNameError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelNameError.ForeColor = System.Drawing.Color.Red;
            this.labelNameError.Location = new System.Drawing.Point(127, 258);
            this.labelNameError.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNameError.Name = "labelNameError";
            this.labelNameError.Size = new System.Drawing.Size(40, 18);
            this.labelNameError.TabIndex = 17;
            this.labelNameError.Text = "error";
            // 
            // UserRegisteration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(185)))), ((int)(((byte)(187)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(965, 678);
            this.Controls.Add(this.labelNameError);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.usernameLabel);
            this.Controls.Add(this.usernameField);
            this.Controls.Add(this.usernameFieldrrorLabel);
            this.Controls.Add(this.AddressErrorLabel);
            this.Controls.Add(this.userPasswordLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.emailField);
            this.Controls.Add(this.registerationFeedbackLabel);
            this.Controls.Add(this.emailFieldError);
            this.Controls.Add(this.signUpButton);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.passwordField);
            this.Controls.Add(this.passwordErrorLabel);
            this.Controls.Add(this.addressField);
            this.Controls.Add(this.password2Label);
            this.Controls.Add(this.password2Field);
            this.Controls.Add(this.password2ErrorLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UserRegisteration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sign Up";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label signUpRedirectLink;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label registerationFeedbackLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox usernameField;
        private System.Windows.Forms.TextBox password2Field;
        private System.Windows.Forms.Label password2ErrorLabel;
        private System.Windows.Forms.Label password2Label;
        private System.Windows.Forms.Label passwordErrorLabel;
        private System.Windows.Forms.TextBox passwordField;
        private System.Windows.Forms.Label PasswordLabel;
        private System.Windows.Forms.Label emailFieldError;
        private System.Windows.Forms.TextBox emailField;
        private System.Windows.Forms.Label userPasswordLabel;
        private System.Windows.Forms.TextBox addressField;
        private System.Windows.Forms.Label usernameFieldrrorLabel;
        private System.Windows.Forms.Button signUpButton;
        private System.Windows.Forms.Label AddressErrorLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelNameError;
    }
}