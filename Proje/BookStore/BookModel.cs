﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
 * @author  : Valerian Kasibante
 * @number  : 152120181099
 * @mail    : kasiabantevalerian25@gmail.com
 * @date    : 29/05/2021
 * @brief   : this class reperesents the books properties
 */

namespace BookStore
{
    public class BookModel : Product
    {
        

        //BooksAttribute
        public string ISBNumber { get; set; }
        public string Author { get; set; }
        public string Publisher { get; set; }
        public int Page { get; set; }

        /// <summary> this function constructor.
        /// </summary>

        public BookModel(string name, string id, int price, string picture, string isbn, string author, string publisher, int pages)
        {
            this.name = name;
            this.id = id;
            this.price = price;
            this.picture = picture;
            ISBNumber = isbn;
            Author = author;
            Publisher = publisher;
            Page = pages;
        }
        /// <summary> this function constructor.
        /// </summary>
        public BookModel(string _name, int _price, string _ID, string _coverPage)
        {
            base.name = _name;
            base.price = _price;
            base.id = _ID;
            base.picture = _coverPage;
        }

        /// <summary> this function print infos.
        /// </summary>
        public override string printProperties()
        {
            return String.Format($"{name} - {id} - {price} - {ISBNumber} - {Author} - {Publisher} - {Page}");
        }
    }
}
