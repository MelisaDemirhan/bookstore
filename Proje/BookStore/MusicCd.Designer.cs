﻿
namespace BookStore
{
    partial class MusicCd
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicCd));
            this.labelMusicCDPrice = new System.Windows.Forms.Label();
            this.labelProductNumber = new System.Windows.Forms.Label();
            this.btnSubtractCD = new System.Windows.Forms.Button();
            this.btnAddCD = new System.Windows.Forms.Button();
            this.lblISBN = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.labelSinger = new System.Windows.Forms.Label();
            this.labelMusicCDName = new System.Windows.Forms.Label();
            this.img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.img)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMusicCDPrice
            // 
            this.labelMusicCDPrice.AutoSize = true;
            this.labelMusicCDPrice.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelMusicCDPrice.ForeColor = System.Drawing.Color.Black;
            this.labelMusicCDPrice.Location = new System.Drawing.Point(72, 325);
            this.labelMusicCDPrice.Name = "labelMusicCDPrice";
            this.labelMusicCDPrice.Size = new System.Drawing.Size(0, 33);
            this.labelMusicCDPrice.TabIndex = 47;
            // 
            // labelProductNumber
            // 
            this.labelProductNumber.AutoSize = true;
            this.labelProductNumber.BackColor = System.Drawing.Color.Transparent;
            this.labelProductNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelProductNumber.ForeColor = System.Drawing.Color.Black;
            this.labelProductNumber.Location = new System.Drawing.Point(112, 367);
            this.labelProductNumber.Name = "labelProductNumber";
            this.labelProductNumber.Size = new System.Drawing.Size(17, 18);
            this.labelProductNumber.TabIndex = 46;
            this.labelProductNumber.Text = "0";
            // 
            // btnSubtractCD
            // 
            this.btnSubtractCD.BackColor = System.Drawing.Color.Transparent;
            this.btnSubtractCD.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSubtractCD.BackgroundImage")));
            this.btnSubtractCD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSubtractCD.FlatAppearance.BorderSize = 0;
            this.btnSubtractCD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubtractCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSubtractCD.Location = new System.Drawing.Point(76, 367);
            this.btnSubtractCD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSubtractCD.Name = "btnSubtractCD";
            this.btnSubtractCD.Size = new System.Drawing.Size(31, 21);
            this.btnSubtractCD.TabIndex = 45;
            this.btnSubtractCD.Text = "-";
            this.btnSubtractCD.UseVisualStyleBackColor = false;
            this.btnSubtractCD.Click += new System.EventHandler(this.btnSubtractCD_Click);
            // 
            // btnAddCD
            // 
            this.btnAddCD.BackColor = System.Drawing.Color.Transparent;
            this.btnAddCD.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddCD.BackgroundImage")));
            this.btnAddCD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddCD.FlatAppearance.BorderSize = 0;
            this.btnAddCD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAddCD.Location = new System.Drawing.Point(136, 367);
            this.btnAddCD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddCD.Name = "btnAddCD";
            this.btnAddCD.Size = new System.Drawing.Size(31, 21);
            this.btnAddCD.TabIndex = 44;
            this.btnAddCD.UseVisualStyleBackColor = false;
            this.btnAddCD.Click += new System.EventHandler(this.btnAddCD_Click);
            // 
            // lblISBN
            // 
            this.lblISBN.AutoSize = true;
            this.lblISBN.BackColor = System.Drawing.Color.Transparent;
            this.lblISBN.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblISBN.ForeColor = System.Drawing.Color.White;
            this.lblISBN.Location = new System.Drawing.Point(107, 367);
            this.lblISBN.Name = "lblISBN";
            this.lblISBN.Size = new System.Drawing.Size(0, 21);
            this.lblISBN.TabIndex = 43;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelType.ForeColor = System.Drawing.Color.Black;
            this.labelType.Location = new System.Drawing.Point(72, 289);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(0, 33);
            this.labelType.TabIndex = 42;
            // 
            // labelSinger
            // 
            this.labelSinger.AutoSize = true;
            this.labelSinger.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelSinger.ForeColor = System.Drawing.Color.Black;
            this.labelSinger.Location = new System.Drawing.Point(72, 258);
            this.labelSinger.Name = "labelSinger";
            this.labelSinger.Size = new System.Drawing.Size(0, 33);
            this.labelSinger.TabIndex = 41;
            // 
            // labelMusicCDName
            // 
            this.labelMusicCDName.AutoSize = true;
            this.labelMusicCDName.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.labelMusicCDName.ForeColor = System.Drawing.Color.Black;
            this.labelMusicCDName.Location = new System.Drawing.Point(72, 225);
            this.labelMusicCDName.Name = "labelMusicCDName";
            this.labelMusicCDName.Size = new System.Drawing.Size(0, 33);
            this.labelMusicCDName.TabIndex = 40;
            // 
            // img
            // 
            this.img.Location = new System.Drawing.Point(72, 43);
            this.img.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.img.Name = "img";
            this.img.Size = new System.Drawing.Size(165, 165);
            this.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.img.TabIndex = 35;
            this.img.TabStop = false;
            // 
            // MusicCd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.labelMusicCDPrice);
            this.Controls.Add(this.labelProductNumber);
            this.Controls.Add(this.btnSubtractCD);
            this.Controls.Add(this.btnAddCD);
            this.Controls.Add(this.lblISBN);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelSinger);
            this.Controls.Add(this.labelMusicCDName);
            this.Controls.Add(this.img);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MusicCd";
            this.Size = new System.Drawing.Size(323, 412);
            ((System.ComponentModel.ISupportInitialize)(this.img)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelMusicCDPrice;
        private System.Windows.Forms.Label labelProductNumber;
        private System.Windows.Forms.Button btnSubtractCD;
        private System.Windows.Forms.Button btnAddCD;
        private System.Windows.Forms.Label lblISBN;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelSinger;
        private System.Windows.Forms.Label labelMusicCDName;
        private System.Windows.Forms.PictureBox img;
    }
}
