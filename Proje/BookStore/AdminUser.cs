﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for adding  user to database, updating user from database, and deleting music Cds from database.
 */
namespace BookStore
{
    public partial class AdminUser : UserControl
    {
        public AdminUser()
        {
            InitializeComponent();
        }
        private string customerIDx, usernamex, addressx, emailx, passwordx, isAdminx, namex;
        SqlQueries sq = new SqlQueries();
        public static List<CustomerModel> FillCustomerList()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.CustomerList();
            return sq1.getCustomerList();
        }

        /// <summary> Function to load new picture for musicCds.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdminUser_Load(object sender, EventArgs e)
        {
            try
            {
                List<CustomerModel> customers = FillCustomerList();
                ListCustomers(customers);
                dataGridView1.Show();
                dataGridView1.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to list paramref name="customers".
        /// </summary>
        /// <param name="customers"></param>

        private void ListCustomers(List<CustomerModel> customers)
        {
            try
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < customers.Count; i++)
                {
                    CustomerModel c = new CustomerModel(usernamex, emailx, passwordx, addressx, customerIDx, Convert.ToBoolean(isAdminx), namex);
                    c = (CustomerModel)customers[i];
                    dataGridView1.Rows.Add();
                    dataGridView1.Rows[i].Cells[0].Value = c.Name;
                    dataGridView1.Rows[i].Cells[1].Value = c.UserName;
                    dataGridView1.Rows[i].Cells[2].Value = c.UserID;
                    dataGridView1.Rows[i].Cells[3].Value = c.UserEmail;
                    dataGridView1.Rows[i].Cells[4].Value = c.UserAddress;
                    dataGridView1.Rows[i].Cells[5].Value = c.IsAdmin;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
