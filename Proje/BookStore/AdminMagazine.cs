﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for adding  magazines to database, updating magazines from databsse, and deleting magazines from database.
 */
namespace BookStore
{
    public partial class AdminMagazine : UserControl
    {
        public AdminMagazine()
        {
            InitializeComponent();
        }
        string picx;
        public static int index_update = -1;
        string namex, idx, issuex, typex;
        int pricex;
        public static int selected_index = -1;
        string name, price, issue, type, id, image;

        SqlQueries db = new SqlQueries();

        /// <summary> Function to make operations when form load. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void AdminMagazine_Load(object sender, EventArgs e)
        {
            try
            {
                List<Product> MList = UploadMagazine();
                ListMagazine(MList);
                dataGridView1.Show();
                dataGridView1.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to list magazines.. 
        /// </summary>
        /// <param name="_magazines"></param>
        private void ListMagazine(List<Product> _magazines)
        {
            try
            {
                dataGridView1.Rows.Clear();
                for (int i = 0; i < _magazines.Count; i++)
                {
                    MagazineModel tmp = new MagazineModel(namex, idx, pricex, picx, issuex, typex);
                    tmp = (MagazineModel)_magazines[i];
                    dataGridView1.Rows.Add();

                    dataGridView1.Rows[i].Cells[0].Value = tmp.id;
                    dataGridView1.Rows[i].Cells[1].Value = tmp.name;
                    dataGridView1.Rows[i].Cells[2].Value = tmp.price;
                    dataGridView1.Rows[i].Cells[3].Value = tmp.issue;
                    dataGridView1.Rows[i].Cells[4].Value = tmp.type;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to add new magazines to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Add");
            try
            {
                List<Product> MList = UploadMagazine();
                for (int i = 0; i < MList.Count; i++)
                {
                    MagazineModel tmp = new MagazineModel(namex, idx, pricex, picx, issuex, typex);
                    tmp = (MagazineModel)MList[i];
                    if (tmp.name == textBoxName.Text)
                    {
                        MessageBox.Show(tmp.name + " ID already exists! Please change!");
                        return;
                    }
                }
                if (textBoxName.Text != "" && textBoxIssue.Text != "" && textBoxPrice.Text != "" && pictureBox1.Image != null && textBoxURL.Text != "")
                {
                    string name = textBoxName.Text;
                    string price = textBoxPrice.Text;
                    string issue = textBoxIssue.Text;
                    string type = comboBoxType.SelectedItem.ToString();
                    string image = textBoxURL.Text;
                    MagazineModel book = new MagazineModel(name, "", Int32.Parse(price), image, issue, type);
                    db.AddMagazine(book);
                    ListMagazine(MList);
                }
                else
                {
                    MessageBox.Show("Please! Fill all fields!");
                }
                ClearTextBox();
                AdminMagazine_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to get infos selected index on the datagridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                List<Product> BKList = UploadMagazine();
                if (BKList.Count != 0)
                {
                    index_update = e.RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index_update];
                    textBoxName.Text = row.Cells[1].Value.ToString();
                    textBoxIssue.Text = row.Cells[3].Value.ToString();
                    textBoxPrice.Text = row.Cells[2].Value.ToString();
                    comboBoxType.SelectedItem = row.Cells[4].Value.ToString();
                    foreach (var item in BKList)
                    {
                        if (item.id == row.Cells[0].Value.ToString())
                        {
                            textBoxURL.Text = item.picture;
                            pictureBox1.Load(item.picture);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Magazine is empty!");
                    buttonDelete.Enabled = false;
                    buttonUpdate.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to load new picture for magazines.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Load");
            try
            {
                pictureBox1.Load(textBoxURL.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to delete  magazines from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Delete");
            try
            {
                dataGridView1.FirstDisplayedCell = null;
                dataGridView1.ClearSelection();
                List<Product> MList = UploadMagazine();
                if (MList.Count != 0)
                {
                    for (int i = 0; i < MList.Count; i++)
                    {
                        selected_index = dataGridView1.CurrentCell.RowIndex;

                        name = dataGridView1.Rows[selected_index].Cells[1].Value.ToString();
                        price = dataGridView1.Rows[selected_index].Cells[2].Value.ToString();
                        issue = dataGridView1.Rows[selected_index].Cells[3].Value.ToString();
                        type = dataGridView1.Rows[selected_index].Cells[4].Value.ToString();
                        id = dataGridView1.Rows[selected_index].Cells[0].Value.ToString();
                        MagazineModel magazine = new MagazineModel(name, id, Int32.Parse(price), "", issue, type);

                        db.DeleteMagazine(magazine);
                        if (MList.Count != 0)
                        {
                            MList.Clear();
                        }
                        dataGridView1.Rows.RemoveAt(selected_index);
                        selected_index = -1;
                        ListMagazine(MList);
                    }
                }
                else
                {
                    MessageBox.Show("Book is Empty!");
                    buttonDelete.Enabled = false;
                    buttonUpdate.Enabled = false;
                }
                ClearTextBox();
                AdminMagazine_Load(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary> Function to clear database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }
        /// <summary> Function to update  magazines from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            ButtonLog.Log("Update");
            try
            {
                selected_index = dataGridView1.CurrentCell.RowIndex;
                name = textBoxName.Text;
                price = textBoxPrice.Text;
                issue = textBoxIssue.Text;
                type = comboBoxType.SelectedItem.ToString();
                id = dataGridView1.Rows[selected_index].Cells[0].Value.ToString();
                image = textBoxURL.Text;
                MagazineModel magazine = new MagazineModel(name, id, Int32.Parse(price), image, issue, type);
                db.UpdateMagazine(magazine);
                List<Product> BKList = UploadMagazine();
                if (BKList.Count != 0)
                {
                    BKList.Clear();
                }
                dataGridView1.Show();
                selected_index = -1;
                ListMagazine(BKList);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            AdminMagazine_Load(sender, e);
        }

        /// <summary>
        ///Function to clear textboxes.
        /// </summary>
        private void ClearTextBox()
        {
            textBoxIssue.Text = "";
            textBoxName.Text = "";
            textBoxPrice.Text = "";
            textBoxURL.Text = "";
            comboBoxType.SelectedItem = "";
            comboBoxType.SelectedIndex = -1;
            pictureBox1.Image = null;
        }
        /// <summary>
        ///Function to get magazines infos from database.
        /// </summary>
        public static List<Product> UploadMagazine()
        {
            SqlQueries sq1 = new SqlQueries();
            sq1.MagazineList();
            return sq1.getMagazineList();
        }

    }
}
