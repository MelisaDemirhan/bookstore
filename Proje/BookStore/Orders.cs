﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
 *@author : Melisa Demirhan
 *@number : 152120181017
 *@mail   : melisa.dmrhn2202@gmail.com
 *@date   : 02.06.2021 
 *@brief  : class for directed to selected option.
 */
namespace BookStore
{
    public partial class Orders : UserControl
    {
        public Orders()
        {
            InitializeComponent();
            
        }
        SqlQueries db = new SqlQueries();

        /// <summary> Function to load bills to datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Orders_Load(object sender, EventArgs e)
        {
            try
            {
                db.OrderList();
                
                dataGridViewBill.DataSource = db.getOrderSource();               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary> Function to load bills to datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewBill_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dataGridViewBill.Columns[0].Index && e.RowIndex >= 0)
                {
                    db.OrderDetailList(int.Parse(dataGridViewBill.Rows[e.RowIndex].Cells[0].Value.ToString()));
                    dataGridViewProduct.DataSource = db.getOrderDetailSource();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
