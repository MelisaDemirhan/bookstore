﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
 *@author : Özge Katırcı
 *@number : 152120181021
 *@mail   : ozgekatirci0@gmail.com
 *@date   : 5-29-2021
 *@brief  : this class for shopping cart. 
 */
namespace BookStore
{
    public class ShoppingCart
    {
        private static List<ItemToPurchase> itemsToPurchase = new List<ItemToPurchase>();
        private string CustomerId;
        private double paymentAmount;
        private string paymentType;
        /// <summary>
        /// This functions are getters and setters.
        /// </summary>
        public string CustomerId1 { get => CustomerId; set => CustomerId = value; }
        public double PaymentAmount { get => paymentAmount; set => paymentAmount = value; }
        public string PaymentType { get => paymentType; set => paymentType = value; }
        public static List<ItemToPurchase> ItemsToPurchase { get => itemsToPurchase; set => itemsToPurchase = value; }

        public NumberFormatInfo p = new NumberFormatInfo();
        /// <summary>
        /// This function is constructor.
        /// </summary>
        /// <param name="_CustomerId">This parameter is customer's ID.</param>
        /// <param name="_paymentAmount">This parameter is payment amount.</param>
        /// <param name="_paymentType">This parameter is payment type.</param>
        /// <param name="_ItemsToPurchase">This parameter is item to purchase page.</param>
        public ShoppingCart(string _CustomerId, double _paymentAmount, string _paymentType, List<ItemToPurchase> _ItemsToPurchase)
        {
            CustomerId = _CustomerId;
            paymentAmount = _paymentAmount;
            paymentType = _paymentType;
            ItemsToPurchase = _ItemsToPurchase;
        }
        /// <summary>
        /// This function prints the products to added purchase.
        /// </summary>
        public string printProducts()
        {
            string ProductInfo = "";
            foreach (var item in ItemsToPurchase)
            {
                ProductInfo += "Product Name :" + item.Product.name + Environment.NewLine
                    + "Product Id :" + item.Product.id + Environment.NewLine
                    + "Product Cost :" + item.Product.price + Environment.NewLine
                    + "Number of products :" + item.Quantity + Environment.NewLine;
            }
            return ProductInfo;
        }
        /// <summary>
        /// This function created for add product to shopping cart.
        /// </summary>
        /// <param name="addProduct">Tproduct that adding  to cart.</param>
        public void addProduct(ItemToPurchase addProduct)
        {
            try
            {
                p.NumberDecimalSeparator = ".";
                foreach (ItemToPurchase item in ItemsToPurchase)
                {
                    if (item.Product.name == addProduct.Product.name)
                    {
                        paymentAmount += addProduct.Quantity * double.Parse(item.Product.price.ToString(), p);
                        item.Quantity += addProduct.Quantity;
                        return;
                    }
                }
                ItemsToPurchase.Add(addProduct);
                paymentAmount += addProduct.Quantity * double.Parse(addProduct.Product.price.ToString(), p);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function created for  remove product .
        /// </summary>
        /// <param name="removeProduct"> product that deleting from cart .</param>
        public void removeProduct(ItemToPurchase removeProduct)
        {
            try
            {
                p.NumberDecimalSeparator = ".";

                foreach (ItemToPurchase item in ItemsToPurchase)
                {
                    if (item.Product.name == removeProduct.Product.name)
                    {
                        if (item.Quantity > removeProduct.Quantity)
                        {
                            paymentAmount -= removeProduct.Quantity * double.Parse(item.Product.price.ToString(), p);
                            item.Quantity -= removeProduct.Quantity;
                            return;
                        }
                        else
                        {
                            paymentAmount -= item.Quantity * double.Parse(item.Product.price.ToString(), p);
                            ItemsToPurchase.Remove(item);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function created for making operations when order completed.
        /// </summary>
        public void placeOrder()
        {
            paymentAmount = 0;
            ItemsToPurchase.Clear();
        }
        /// <summary>
        /// This function deletes the products from shopping cart.
        /// </summary>
        public void cancelOrder()
        {
            paymentAmount = 0;
            ItemsToPurchase.Clear();
        }
        /// <summary>
        /// This function sends e-invoice to customer.
        /// </summary>
        public void sendInvoidcebyEmail()
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("polymathfatura@outlook.com");
                mail.To.Add(ActiveUser.getInstance().Customer.UserEmail);
                mail.Subject = "PolyMath Invoice";
                mail.Body = "Thank you for choosing us, your bill.";
                SmtpClient server = new SmtpClient();
                server.Port = 587;
                server.Host = "smtp.live.com";
                server.Credentials = new System.Net.NetworkCredential("polymathfatura@outlook.com", "codemakelifediff152");
                server.EnableSsl = true;

                Attachment item = new Attachment("Invoice.pdf");
                mail.Attachments.Add(item);
                server.Send(mail);
                MessageBox.Show("E-invoice sent to your mail.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// This function sends invoice to phone.
        /// </summary>
        public void sendInvoicebySMS()
        {
            MessageBox.Show("Invoice sent to your phone.");
        }
    }
}